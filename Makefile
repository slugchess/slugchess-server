#SlugChess Server is a server for SlugChess variants
#Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

LDFLAGS = -L/usr/local/lib `pkg-config --libs protobuf grpc++`\
           -Wl,--no-as-needed -lgrpc++_reflection -Wl,--as-needed\
           -ldl -rdynamic

# INSTALL THIS FOR fmt::format sudo apt install libfmt-dev
CXX = g++ -std=c++20
#CPPFLAGS += `pkg-config --cflags protobuf grpc`
HEADERS = -I$(ROOT_DIR)/slugchess-core/src -I$(ROOT_DIR)/chesscom
CXXFLAGS += -O2 -Wall -DFMT_HEADER_ONLY
CXXFLAGS_DBG += -g -Wall -DDEBUG -DFMT_HEADER_ONLY

CXXFLAGS_PB += -O2

GRPC_CPP_PLUGIN = grpc_cpp_plugin
GRPC_CPP_PLUGIN_PATH ?= `which $(GRPC_CPP_PLUGIN)`

PROTOS_PATH = proto
vpath %.proto $(PROTOS_PATH)
ROOT_DIR=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
SC_LIB = $(ROOT_DIR)/slugchess-core/output
SHARED_LIB_OUT = /usr/local/lib
SRC  = src
OUT = output
OBJ = object
DEP = depends
CHESSCOM = chesscom
$(shell mkdir -p $(OUT) )
$(shell mkdir -p $(OBJ) )
$(shell mkdir -p $(DEP) )
$(shell mkdir -p $(CHESSCOM) )
SOURCES := $(wildcard $(SRC)/*.cpp)
OBJECTS := $(patsubst $(SRC)/%.cpp,$(OBJ)/%.o,$(SOURCES))
DEPENDS := $(patsubst $(SRC)/%.cpp,$(OBJ)/%.d,$(SOURCES))
OBJECTS_DBG := $(patsubst $(SRC)/%.cpp,$(OBJ)/%.od,$(SOURCES))
CHESSCOM_OBJ = $(OBJ)/chesscom.pb.o $(OBJ)/chesscom.grpc.pb.o

# .PHONY means these rules get executed even if
# files of those names exist.
.PHONY: all clean clean-output

all: slugchess-server
dockerlib:
	cd slugchess-core && $(MAKE) export

slugchess-server: clean-output $(CHESSCOM_OBJ) $(OUT)/slugchess-server
slugchess-server-dbg: $(CHESSCOM_OBJ) $(SC_LIB)/libslugchesscored.so $(OUT)/slugchess-server-dbg

run-debugserver: slugchess-server-dbg copy-over-database
#	GRPC_VERBOSITY=DEBUG GRPC_TRACE=tcp,http,api ./$(OUT)/slugchess-server-dbg --port 43326	
	GRPC_VERBOSITY=DEBUG ./$(OUT)/slugchess-server-dbg --port 43326

copy-over-database:
	rsync --ignore-errors --ignore-non-existing --mkpath -a debug-games_database/ output/data/games_database/
	rsync --ignore-errors --ignore-non-existing --mkpath -a debug-user_database/ output/data/user_database/

$(SHARED_LIB_OUT)/libslugchesscore.so: 
	echo Building SlugChess shared lib. NO do this youself lazy man. Only need to exist runtome
#	cd ../SlugChessCore && $(MAKE) sudo_export  

$(SC_LIB)/libslugchesscored.so: 
#	echo Building SlugChess local lib. NO do this youself lazy man. Only need to exist runtome
	echo Building Slugchess Core && cd slugchess-core && $(MAKE) slugchesscored

$(OUT)/slugchess-server: $(CHESSCOM_OBJ) $(SHARED_LIB_OUT)/libslugchesscore.so $(OBJECTS) 
	$(CXX) $^ $(LDFLAGS) -L$(SHARED_LIB_OUT) -Wl,--enable-new-dtags -Wl,-rpath=$(SHARED_LIB_OUT) -o $@ -l:libslugchesscore.so.0

$(OUT)/slugchess-server-dbg: $(CHESSCOM_OBJ) $(OBJECTS_DBG) 
	$(CXX) $^ $(LDFLAGS) -L$(SC_LIB) -Wl,--enable-new-dtags -Wl,-rpath=$(SC_LIB) -o $@ -l:libslugchesscored.so.0




-include $(DEPENDS)

.PRECIOUS: $(CHESSCOM)/%.grpc.pb.cc
$(CHESSCOM)/%.grpc.pb.cc: %.proto
	protoc -I $(PROTOS_PATH)  --grpc_out=$(CHESSCOM) --plugin=protoc-gen-grpc=$(GRPC_CPP_PLUGIN_PATH) $<

.PRECIOUS: $(CHESSCOM)/%.pb.cc
$(CHESSCOM)/%.pb.cc: %.proto
	protoc -I $(PROTOS_PATH)  --cpp_out=$(CHESSCOM) $<


clean-output:
	rm -rf $(OUT)/slugchess*

clean-all:
	rm -rf $(OBJ)/* $(DEP)/* $(CHESSCOM)/* $(OUT)/* *.log null.d

clean: clean-output
	rm -rf $(OBJ)/* $(DEP)/* $(CHESSCOM)/* null.d

$(OBJ)/%.pb.o: $(CHESSCOM)/%.pb.cc Makefile 
	$(CXX) $(CXXFLAGS_PB) $(LDFLAGS) $(HEADERS) -MMD -MP -c $< -o $@

$(OBJ)/%.grpc.pb.o: $(CHESSCOM)/%.grpc.pb.cc $(CHESSCOM)/%.pb.cc Makefile 
	$(CXX) $(CXXFLAGS_PB) $(LDFLAGS) $(HEADERS) -MMD -MP -c $< -o $@

$(OBJ)/%.o: $(SRC)/%.cpp Makefile $(CHESSCOM_OBJ)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(HEADERS) -MMD -MP -c $< -o $@

$(OBJ)/%.od: $(SRC)/%.cpp Makefile $(CHESSCOM_OBJ)
	$(CXX) $(CXXFLAGS_DBG) $(LDFLAGS) $(HEADERS) -MMD -MP -c $< -o $@


