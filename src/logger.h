/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include <string>
#include <chrono>
#include <iostream>
#include <fstream>
#include <sstream>
#include <mutex>
#include <fmt/format.h>

class Logger
{
private:
    static std::ofstream _logfile;
    static std::string _name_logfile;
    static bool _log_to_console;
    static std::mutex _mutex;
public:
enum LogLevel {
        Debug       = 1,
        Info        = 2,
        Warning     = 3,
        Error       = 4
    };
    static void setup(bool logToConsole, const std::string& logToFile = "")
    {
        if(logToFile != ""){
            _logfile = std::ofstream(logToFile, std::ios::trunc);
            if (_logfile.is_open())
            {
                _logfile << "SlugChessServinator Logfile Start";
                _name_logfile = logToFile;
                //myfile.close();
            }else {
                _name_logfile = "";
            }
        }else{
            _name_logfile = "";
        }
        _log_to_console = logToConsole;
        
    }
    static void log(enum LogLevel ll, const std::string& message){
        std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

        std::string time(30, '\0');
        std::strftime(&time[0], time.size(), "%Y-%m-%d %H:%M:%S", std::localtime(&now));
        std::stringstream ss;
        ss << time << "-" << logLevelToString(ll) << ": " << message << std::endl;
        std::string out_string = ss.str();

        std::scoped_lock<std::mutex> lock(_mutex);
        if(_name_logfile !=  ""){
            _logfile << out_string;
            _logfile.flush();
        }
        if(_log_to_console){
            std::cout << out_string;
            std::cout.flush();
        }

    }

    static std::string logLevelToString(enum LogLevel ll){
        switch (ll)
        {
        case Debug:
            return "Debug";
        case Info:
            return "Info";
        case Warning:
            return "Warning";
        case Error:
            return "Error";
        }
        throw std::invalid_argument("WTF have you set as LogLevel");
    }
};
