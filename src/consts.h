/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#define MAX_SLEEP_MS 1000
#define SHUTDOWN_WAIT_MS static_cast<int>(5.5 * MAX_SLEEP_MS)
#define SECRET "secret"
#define DATETIME_FORMAT "%Y-%m-%d-%H-%M-%S"
#define DATETIME_PGN_FORMAT "%Y.%m.%d%H:%M:%S"
#define DATABASE_MANAGER_GAMES_PAGE_SIZE 50
#define MOVE_TIME_WIGGLEROOM_MS 50
#define MOVE_TIME_MAX_ASSUMED_PING_MS 1000
 
// namespace Const {
//     static const int32_t MAX_SLEEP_MS = 1000;
//     static const int32_t SHUTDOWN_WAIT_MS = 2.5 * MAX_SLEEP_MS;
// }