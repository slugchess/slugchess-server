/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include <mutex>
#include <memory>
#include <algorithm>
#include <list>
#include <iterator>

#include "../chesscom/chesscom.grpc.pb.h"

#include "consts.h"
#include "common.h"
#include "filesystem.h"

class DatabaseManager
{
private:
    struct QueryResult{
        std::string id;
        chesscom::GameSearch query;
        time_point timestamp;
        std::vector<chesscom::PgnInfo> games;
    };

    static DatabaseManager* _instance;
    DatabaseManager(){ }

    const uint16_t PAGE_SIZE = DATABASE_MANAGER_GAMES_PAGE_SIZE;
    std::mutex _mutex;
    std::list<std::shared_ptr<QueryResult>> _query_results;

public:
    static DatabaseManager* Get()
    {
        if(!_instance) _instance = new DatabaseManager();
        return _instance;
    }
    void DoWork();

    [[nodiscard]] size_t SavedQueriesCount(){ return _query_results.size(); }
    chesscom::GamesDatabaseResult QueryGamesDatabase(const chesscom::GameSearch& query);
    chesscom::GamesDatabaseResult GamesDatabaseFetch(const chesscom::GamesDatabaseFetch& fetch);

};