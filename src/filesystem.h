/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include <string>
#include <iostream>
#include <chrono>
#include <fstream>
#include <sstream>
#include <filesystem>
#include <unistd.h>
#include <iomanip>
#include <google/protobuf/util/json_util.h>
#include <google/protobuf/util/time_util.h>

#include "../include/plusaes/plusaes.hpp"

#include "../chesscom/chesscom.grpc.pb.h"
#include "consts.h"
#include "common.h"
#include "messenger.h"
#include "logger.h"
#include "threadpool.h"

class Filesystem
{
private:
    static std::string _root_dir_cached;
    static std::string _data_dir;
public:
    static inline bool ReadTime(const std::string& s, time_t& t)
    {
        std::tm tm;
        std::istringstream ss(s);
        ss >> std::get_time(&tm, DATETIME_FORMAT); 
        if (ss.fail()) return false;
        t = mktime(&tm);
        return true;
    }
    static inline bool ReadPgnTime(const std::string& s, time_t& t)
    {
        std::tm tm;
        std::istringstream ss(s);
        ss >> std::get_time(&tm, DATETIME_PGN_FORMAT); 
        if (ss.fail()) return false;
        t = mktime(&tm);
        return true;
    }
    static std::string ReadFile(const std::filesystem::path& path);
    static std::string ExeDir();
    static std::string DataDir();
    static void SetDataDir(const std::string path);

    static void WriteMatchPgn(const std::string userWhite, const std::string& userBlack, const std::string& pgn, time_t& time_t);
    static std::vector<chesscom::PgnInfo> GetListOfGamesDatabase();
    static std::string GetPgnFromGamesDatabase(const std::string& filename);
    static chesscom::PgnInfo PgnPathToInfo(const std::filesystem::path& path);
    static bool UserFileExists(const std::string& filename);

    // encryprion key must be 32 chars long.
    static void WriteUserFile(chesscom::UserStaticData& userdata, const std::string& filename);
    static chesscom::UserStaticData ReadUserFile(const std::string& filename);
    static bool OpenFileWithAttemps(const std::string* filepath, std::ifstream* dataFile, const std::ios_base::openmode openmode);
};
