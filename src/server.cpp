/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#include <string>
#include <csignal>
#include <unordered_map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <queue>
#include <vector>
#include <mutex>
#include <random>
#include <chrono>
#include <filesystem>
#include <thread>
#include <condition_variable>
#include <stdexcept>
#include <execinfo.h>
#include <fmt/printf.h>

#include <google/protobuf/util/time_util.h>
#include <grpcpp/grpcpp.h>
#include "../chesscom/chesscom.grpc.pb.h"
#include "slugchess.h"

#include "version.h"
#include "consts.h"
#include "chesscomservice.h"
#include "worker.h"
#include "filesystem.h"
#include "databasemanager.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

std::atomic<bool> orderedShutdown(false);
std::sig_atomic_t signaled = 0;
std::unique_ptr<Server> server;
static std::ofstream logFile;
static std::ofstream cfgFile;

std::string localhost_cert = R"(
-----BEGIN CERTIFICATE-----
MIIFLTCCAxWgAwIBAgIUQWcy4yReU9vvGzHfuFrpEMFzb+IwDQYJKoZIhvcNAQEL
BQAwGDEWMBQGA1UEAwwNc2x1Z2NoZXNzLmNvbTAeFw0yMzA0MDQxNzQyMDVaFw0z
MzA0MDExNzQyMDVaMBgxFjAUBgNVBAMMDXNsdWdjaGVzcy5jb20wggIiMA0GCSqG
SIb3DQEBAQUAA4ICDwAwggIKAoICAQCxcFXJW6BkTg2B+EhKsMf/QtKF0YdtX/UL
Wo/YtLCdZoVGOd5phqTg0N8A2ZzGEsZjm0FlN0vn2fGD2Q080Cb9SSgeXjEPg1rI
FBi3jLENUVKENPf/o+f2aG/Ibb5Za/nw6n8akQ7W2b5o+m/iSRJH9hRHFJHDfAcq
BpSJHELHvSoI+Ll/FrpUbdMo5XCHc2vxzGURfOgx48rnkb+Btzkl37O/TqAmr4kQ
q+d0xHp3og7RDbNU9VXXd6t6wxt5DGopGDwnXAY4xJA+Mih0aOw6+bFbN0OELzZr
SC1b2o2DYyPJphNrtqlKdt4E0S7fQS4W8nSd2EMGXMP83jdjbJOk1rmn3ajz9sYN
bWTlofFb49EuGJ9UrXbnoT1NmHvMLQVDYONHgbra3V8T62rv05n2jSeALAwZzNuk
Y5r44hkvxa9pWa1L/dhqt9dNwouYjbxXLTfkOos/0vLkyVmr4X+aeVy9PWMqW5TJ
SBnX10nKjPNi8r9gG1sl3ejU1CqomDF6eHRHrULXp8oT13Wi00MOc4obnV0f5Nes
YKvtRNvy87YMaIBwGEPqa9qSkRvcERxr/08lGND+V4gA0JoAP/sHtBgF5561zc+t
fJl9d867PpBamak4DcxmMt1DIwdLkTIhDCurYeUnTc/kbzFylQ9LWwY0my+MLL5T
pZcyyzai2QIDAQABo28wbTAdBgNVHQ4EFgQUcC1GwB6bEb8BDqFxPmo5or0dZEEw
HwYDVR0jBBgwFoAUcC1GwB6bEb8BDqFxPmo5or0dZEEwDwYDVR0TAQH/BAUwAwEB
/zAaBgNVHREEEzARgglsb2NhbGhvc3SHBH8AAAEwDQYJKoZIhvcNAQELBQADggIB
AEf0d/9k64HFyJqDpdTFIF7S6ChA4V/DMVTuBKEpkxGPE++SAgAxauAGVmibO638
JYNJcOrhhK1p7av8gf31uXp5FiQGQeQnfja0jG29hZ3+n0GLGf+BRdxwfKzQfmZP
COs1r1L/yvLXChJ/P8QuVT7bsjkFkc0g1kkFHI5TbUn2vZgENUf8/ZqrJ2Gl2YLL
UFA656NZ0gw6VCWUwial/7inkk+aNevNErIMsGffWp2+DKecU73pcL+HBCpbc4ne
IczMyhgkKI7TBfd8JaYejh0so0EbxAptwcM7SdhDVRAqYfi/XO+DMiwHLZ+PaEwO
29iK48WsXhEjQ8pVgXO7IGqKvWg97asNo1EUNK+fYm6Td7pKaj3hCgvPWrU/xhg/
CCWLQYkvjgvovkNjpRbQLQyiETd4FL04KPNnOFGfi3/MsZYHLCL02P4P/Euj5kJd
yjS9qGSOy1pvxc/7BJyCHe79KzfPUUISrF3dgbmkmWTFir4cD8DT1o7dCXjC7zit
wHpi1xYe2bNfmwy6gQB17ZJnE3TY/ormakf/wrauRs3fRTH12CqZX1bTOCabvTT5
jLq8HkWFqafsJBlj/iKrGwtXE5YwUKrKz1kHj+cmYRgErsaKWLnWJjlNGdw1CTsD
UsvMWM0q2xFVDA6mEnm3TCS2NTT9FW8ov8Bqsc3Kii3t
-----END CERTIFICATE-----
)";
std::string localhost_key = R"(
-----BEGIN PRIVATE KEY-----
MIIJQwIBADANBgkqhkiG9w0BAQEFAASCCS0wggkpAgEAAoICAQCxcFXJW6BkTg2B
+EhKsMf/QtKF0YdtX/ULWo/YtLCdZoVGOd5phqTg0N8A2ZzGEsZjm0FlN0vn2fGD
2Q080Cb9SSgeXjEPg1rIFBi3jLENUVKENPf/o+f2aG/Ibb5Za/nw6n8akQ7W2b5o
+m/iSRJH9hRHFJHDfAcqBpSJHELHvSoI+Ll/FrpUbdMo5XCHc2vxzGURfOgx48rn
kb+Btzkl37O/TqAmr4kQq+d0xHp3og7RDbNU9VXXd6t6wxt5DGopGDwnXAY4xJA+
Mih0aOw6+bFbN0OELzZrSC1b2o2DYyPJphNrtqlKdt4E0S7fQS4W8nSd2EMGXMP8
3jdjbJOk1rmn3ajz9sYNbWTlofFb49EuGJ9UrXbnoT1NmHvMLQVDYONHgbra3V8T
62rv05n2jSeALAwZzNukY5r44hkvxa9pWa1L/dhqt9dNwouYjbxXLTfkOos/0vLk
yVmr4X+aeVy9PWMqW5TJSBnX10nKjPNi8r9gG1sl3ejU1CqomDF6eHRHrULXp8oT
13Wi00MOc4obnV0f5NesYKvtRNvy87YMaIBwGEPqa9qSkRvcERxr/08lGND+V4gA
0JoAP/sHtBgF5561zc+tfJl9d867PpBamak4DcxmMt1DIwdLkTIhDCurYeUnTc/k
bzFylQ9LWwY0my+MLL5TpZcyyzai2QIDAQABAoICABthEpOp9ENV+eaaqYkA4zNj
m7lzXdrGKjkQxc5x/yiid6Eg781HrexUvnxSl8rGfd9DxyD3mdpSOl6WYOEH22RM
5Ahfj2Ft6tWhqDi2WGGOMVXeGIBVSccvUURTjsIck6oVfPxb8ra7TZLERfxcOxpo
gbOAHIfV/QMClevcNP++FBzjSDgwiRrbAXC3ipgT+s2KNPQwWmHMXPRDfJHm9EYA
USuNVc/yWQt+Aw50/NK16mReoVPUMWU4fXjJX1Vb0Yu6xhlqrTkYN4upUK25vO3S
toe1LNFaOaLvNbSNMjXaV9zXJ/sAafnoApUqs/lWBOccjVsH0jvKl6FOpqxMRjGw
vuX94y0lupZp3b04qhaeGFP+8MpBrwQpzsnVwey5v0EOh+P/8w3F7w4nccmC1nuf
w4hGdySHynf2WxeO7rysfXPs9Y5cc5azccfsD2Q1KnWcdg3XIiAvlF50krDyUyal
yQO4/eHI8YarW/HJJx+ccVxEThhjgSHB1C+pCsIUm151J0rmFF9btTMPebe7PK9m
aGCzzSUY7m1UxpnIUbmPxXaJFO7XqWKVthUl09sTV62Otdz4rLR2fLz4SDAj80y5
6hhmMy0d2lGu8g+sZTI4kKZj3dfuOCbBPityuiEnb9vWEDGR0hDv4wldEzqaGS21
XfaqpcprT7cCQnTe9RzzAoIBAQDoH1NkbPVPvhvcKml8mwgsvb76pc4H7YOoFsWJ
vZ0ns0N/nfcmXY0JjO0xfU9mNrTh0Tz2gwqUCOKqDi844tjD59ZihdLxJBkTi7FM
vGcyBGn1XGmXtl3UMLOsMy/CfWVQSYWsZxyBrBs/6ydj+KbX+BMx3zYo4kK4M6hA
7+40QYwjLIOaTrHKGbPvUBQs8DocfmUycBt4EpwHJz6ASGzoSFs0Dup4mDf1B62N
pVxMaqvvMIrC6dQ7qIaxOeiBuGLBxKamUNFGahno06mbxzeeB+ze7k8/drGkg7PK
YXAg+o9x8lS7v9K4VGDAqJNpJ92sCzsabxvk2hsQAVtgMn8zAoIBAQDDsPsnf/2Q
+bz54+raB4BAqlkstq0Z4OI8vkrh2ju8wToSiRT4snVu8bRW7AUw+R+dNQXOoZPM
o4ZaOWOTqVtXvy8pmF7cYQCyETmbvHpkJOoLC5U80yc9XJahGiSoHQEm6BEs+WbT
btj4Lidqsw1QYTmpZjcyd/Gqo6gBmxMcZdJ/nRomIsE5LTjxOlK3k/ZFz80lbURE
lM5y9jhLdJqmvJRLM95jmsfDs6v+SSy3f6qpavLs1yYuHsDkBRYoUeRrRgkjUQYV
XbcWeA5m6W/Psst1YseXQm2tGNl1ixBlZP1vPO4L1WQg7pQLlA59b+uWaFkdmxWW
JmmE7cVBrUXDAoIBAQDPtyiS14X14/NeY32eCcWY8/swpKroIT0Nb8M5bTpyylH+
o0cFR/Xrs/aVsMMafp4C7ZCWL9GJgyICd0r+ptC/DnmkFIkdTOY7vWfdeUMHWTfQ
XvAG2STBuwstb2ivrDK7U++QxXyW9YI1AqNvLJVw8geLO2ll0INP/rnz0kDg9j8H
QtBJh15FshZ4crp7NEVsNhcsuXF7U3vYUt/1P9plSkqDHU3ouJ5qYIKIHI+pulM9
abziiwhmknploaJUYsHND8LpVazQTCX/WVmPkwhMHF91Zt/3tKlmxIxg8Cy89jrj
Bq2YjSFt3ePuw2uWUBxn1mz/TKozcpGBExXH0ahNAoIBAHjhg1M0EcsTKbPnITSL
O6M2Vx1yvlzk+HtzRMhcyVfguGGowgaaHkZpEfA7nT1SNkAi97JbRvQ68wwatTMK
g30hgR2dnMNfPFHCqCZWd/gSgWxPPGG1QWD7J6TehFRAjmydH7b7XJQwveAYioqz
jf7gAAFvPZt0UhkJPQe45kY9c5kWz4dNIrSSBcGlOGFkCdW39k4vP+NpVFnZVI7v
uXRTuvr1UGOQweNWwmCyjOfQ+ffrM6IviMsmfhqcNcK0S3gkbBKfwzVBjY5YnPTf
MA1JsbqSSR5bj2pEllewjnFDv/WN2rH+n/pjxZmdXJIJp6piTOIn4V693aKaSaY3
RYcCggEBAMa8ezzp5v3Jmb+LZW4geWo6TiW2qq4RHOh0WpjSBFL/MSisikVgs3FY
cIoYJnZFCDftc+jFJachmnpvgBwZ5OeQ2zncMxA9k15nLSw9ZXQGFZi7iCn5oa0C
QLmD/0loY81FpWSBGGXnkyPKRCPyiG58C6v28SYO+aGyvnDLCFk8PVSkUHc69jaH
VBDUgY+kFGbrjWZyAn6gbrR9Gnqh3ztB3Zp3R/hfaNgrF2umJCOcF14IwkKDvro8
zZjxC2mzoChhPwjQ1k2H+hHrapxGVYmyhnpjY+IUEkgG2ia7BWQ14Av4XEjttLGw
qsutA6VBVBW2M6SeMQLQCLwAU7LSJ58=
-----END PRIVATE KEY-----
)";
// std::string localhost_root = R"(-----BEGIN CERTIFICATE-----
// MIICJjCCAc2gAwIBAgIUO33Vypi98I+OJw3AoV5+JknkMy4wCgYIKoZIzj0EAwIw
// aTELMAkGA1UEBhMCTk8xDzANBgNVBAgMBm1vcmRvcjEPMA0GA1UEBwwGbW9yZG9y
// MRIwEAYDVQQKDAlsb2NhbGhvc3QxEjAQBgNVBAsMCWxvY2FsaG9zdDEQMA4GA1UE
// AwwHUk9PVCBDQTAeFw0yMzAxMzExOTQ1MzBaFw0yNDAyMTUxOTQ1MzBaMGkxCzAJ
// BgNVBAYTAk5PMQ8wDQYDVQQIDAZtb3Jkb3IxDzANBgNVBAcMBm1vcmRvcjESMBAG
// A1UECgwJbG9jYWxob3N0MRIwEAYDVQQLDAlsb2NhbGhvc3QxEDAOBgNVBAMMB1JP
// T1QgQ0EwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAATABr9EDnXxnP66JUk7gLY7
// 8oASBI+AcE/2nMve8+RCCYarEoSlpCp2UXMuZ3dnqy5BmrjRqYEJUY7H22Trxj4B
// o1MwUTAdBgNVHQ4EFgQUY9SeUD7MptVrUXk8OezFUFo/SPowHwYDVR0jBBgwFoAU
// Y9SeUD7MptVrUXk8OezFUFo/SPowDwYDVR0TAQH/BAUwAwEB/zAKBggqhkjOPQQD
// AgNHADBEAiBnHvnZ9GLCgQtYEbDqQiXM2PxsTgyQTXYqOkaCbCo4RAIgHKSNTtrx
// W0MqEfgN2wR0RvehfaUhPnHWKQAcEuBhPzY=
// -----END CERTIFICATE-----)";


#include <stdio.h>
#include <stdlib.h>
#include <cxxabi.h>
/** Print a demangled stack backtrace of the caller function to out. NOT WRITTEN BY ME */
static inline void print_stacktrace(std::ostream& out = std::cerr, unsigned int max_frames = 63)
{
    out << fmt::sprintf("stack trace:\n");
    // storage array for stack trace address data
    void* addrlist[max_frames+1];

    // retrieve current stack addresses
    int addrlen = backtrace(addrlist, sizeof(addrlist) / sizeof(void*));

    if (addrlen == 0) {
        out << fmt::sprintf("  <empty, possibly corrupt>\n");
        return;
    }

    // resolve addresses into strings containing "filename(function+address)",
    // this array must be free()-ed
    char** symbollist = backtrace_symbols(addrlist, addrlen);

    // allocate string which will be filled with the demangled function name
    size_t funcnamesize = 256;
    char* funcname = (char*)malloc(funcnamesize);

    // iterate over the returned symbol lines. skip the first, it is the
    // address of this function.
    for (int i = 1; i < addrlen; i++)
    {
        char *begin_name = 0, *begin_offset = 0, *end_offset = 0;

        // find parentheses and +address offset surrounding the mangled name:
        // ./module(function+0x15c) [0x8048a6d]
        for (char *p = symbollist[i]; *p; ++p)
        {
            if (*p == '(')
            begin_name = p;
            else if (*p == '+')
            begin_offset = p;
            else if (*p == ')' && begin_offset) {
            end_offset = p;
            break;
            }
        }

        if (begin_name && begin_offset && end_offset
            && begin_name < begin_offset)
        {
            *begin_name++ = '\0';
            *begin_offset++ = '\0';
            *end_offset = '\0';

            // mangled name is now in [begin_name, begin_offset) and caller
            // offset in [begin_offset, end_offset). now apply
            // __cxa_demangle():

            int status;
            char* ret = abi::__cxa_demangle(begin_name,
                            funcname, &funcnamesize, &status);
            if (status == 0) {
            funcname = ret; // use possibly realloc()-ed string
            out << fmt::sprintf("  %s : %s+%s\n",
                symbollist[i], funcname, begin_offset);
            } else {
            // demangling failed. Output function name as a C function with
            // no arguments.
            out << fmt::sprintf("  %s : %s()+%s\n",
                symbollist[i], begin_name, begin_offset);
            }
        }else{
            // couldn't parse the line? print the whole line.
            out << fmt::sprintf("  %s\n", symbollist[i]);
        }
    }
    free(funcname);
    free(symbollist);
}

// Very few things are allowed in signal handeling callback. Instead set data for other thread to reed
void SigintHandler (int param)
{
    orderedShutdown = true;
    Logger::log(Logger::LogLevel::Debug, fmt::format("Signal interupt. Fuck yeah. signal {}", strsignal(param)));
}

void SegFaultHandler(int param){
    std::stringstream ss; ss << "Program terminated with segfault.";
    print_stacktrace(ss, 30);
    Logger::log(Logger::LogLevel::Error, ss.str());
    exit(EXIT_FAILURE);
}

void ThreadCheckShutdown()
{
    while (!orderedShutdown) std::this_thread::sleep_for(std::chrono::milliseconds(300));
    Logger::log(Logger::LogLevel::Debug, fmt::format("Ordered Shutdown"));
    Logger::log(Logger::LogLevel::Debug, fmt::format("Stop signal to Worker"));
    Worker::Stop();
    Logger::log(Logger::LogLevel::Debug, fmt::format("Stopping Server"));
    auto deadline = std::chrono::system_clock::now() + std::chrono::milliseconds(SHUTDOWN_WAIT_MS);
    server->Shutdown(deadline);
    //std::this_thread::sleep_for(std::chrono::milliseconds(3000));
    Logger::log(Logger::LogLevel::Debug, fmt::format("Joining Worker"));
    Worker::Join();
}

void Run(std::string port, bool use_tls) {
    //Booting up singeltons
    Worker::Start(); //Kick of work thread.
    MatchManager::Get();
    UserManager::Get();
    GameBrowser::Get();
    DatabaseManager::Get();
    //Add summart every 10 minutes
    Worker::AddWork(std::make_shared<Work>("summary", std::chrono::minutes(1), []()
    {
        //time_t curettTime = time(0);
        Logger::log(Logger::LogLevel::Info, fmt::format("Summary - \n\tCurrent users: {}\n\tCurrent matches: {}\n\tCurrent Games Database saved queries: {}",
            std::to_string(UserManager::Get()->ActiveUsers()), 
            std::to_string(MatchManager::Get()->OngoingMatches()), 
            std::to_string(DatabaseManager::Get()->SavedQueriesCount())));  
    }));
    //Check heartbeat every 2 minutes
    Worker::AddWork(std::make_shared<Work>("heartbeat_check", std::chrono::minutes(2), []()
    {
        //Logger::log(Logger::LogLevel::Debug, fmt::format("Checking Heartbeats"));
        UserManager::Get()->CheckHeartbeat();
    }));
    //Process database evert 1 minute
    Worker::AddWork(std::make_shared<Work>("process_database", std::chrono::minutes(1), []()
    {
        //Logger::log(Logger::LogLevel::Debug, fmt::format("Processing database"));
        DatabaseManager::Get()->DoWork();
    }));

    Worker::AddWork(std::make_shared<Work>("ping_service", std::chrono::seconds(1), []()
    {
        //Logger::log(Logger::LogLevel::Debug, fmt::format("Processing database"));
        UserManager::Get()->CheckPing();
    }));
    Worker::AddWork(std::make_shared<Work>("match_manager", std::chrono::seconds(1), []()
    {
        //Logger::log(Logger::LogLevel::Debug, fmt::format("Processing database"));
        MatchManager::Get()->DoWork();
    }));

    std::string address("0.0.0.0:" + port);

    ChessComService service;
    ServerBuilder builder;
    if(use_tls){
        auto cert_path = std::filesystem::path(Filesystem::ExeDir()+"/certs/server_cert.crt");
        std::string cert = std::filesystem::exists(cert_path)? Filesystem::ReadFile(cert_path) : localhost_cert;
        auto key_path = std::filesystem::path(Filesystem::ExeDir()+"/certs/server_private_key.key");
        std::string key = std::filesystem::exists(key_path)? Filesystem::ReadFile(key_path) : localhost_key;
        //auto root_path = std::filesystem::path(Filesystem::ExeDir()+"/certs/root_cert.crt");
        //std::string root = std::filesystem::exists(root_path)? Filesystem::ReadFile(root_path) : localhost_root;
        grpc::SslServerCredentialsOptions::PemKeyCertPair keycert = { key, cert};
        grpc::SslServerCredentialsOptions sslOps;
        //sslOps.pem_root_certs = root;
        sslOps.pem_key_cert_pairs.push_back ( keycert );

        Logger::log(Logger::LogLevel::Info, fmt::format("SlugChess Server listening on TLS"));
        builder.AddListeningPort(address, grpc::SslServerCredentials(sslOps));
    } else {
        Logger::log(Logger::LogLevel::Info, fmt::format("SlugChess Server listening on insecure"));
        builder.AddListeningPort(address, grpc::InsecureServerCredentials());
    }

    
    builder.RegisterService(&service);

    Logger::log(Logger::LogLevel::Info, fmt::format("SlugChess Server version {} starting", VERSION));
    server = builder.BuildAndStart();
    Logger::log(Logger::LogLevel::Info, fmt::format("SlugChess Server listening on port: {}", address));

    server->Wait();
    Logger::log(Logger::LogLevel::Debug, fmt::format("After wait happened. Do cleanup"));
    UserManager::DeleteInstance();
    
}

int main(int argc, char** argv) {
    Logger::setup(true, Filesystem::ExeDir()+"server.log");
    Logger::log(Logger::LogLevel::Debug, "Writing this to a file.");

    std::vector<std::string> args;
    for(int i = 0; i < argc; i++){
        args.push_back(argv[i]);
    }

    std::string port = "43326";
    std::string data_path = Filesystem::ExeDir() + "/data";
    bool use_tls = true;

    {
        auto it_port = std::find(args.begin(),args.end(), "--port");
        if(it_port != args.end()){
            port = *++it_port;
            Logger::log(Logger::LogLevel::Info, fmt::format("Custom port '{}'", port));
        }
    }
    {
        auto it_data = std::find(args.begin(),args.end(), "--data");
        if(it_data != args.end()){
            data_path = *++it_data;
        }
    }
    {
        auto it_insecure = std::find(args.begin(),args.end(), "--insecure");
        if(it_insecure != args.end()){
            use_tls = false;
        }
    }
    Filesystem::SetDataDir(data_path);
    Logger::log(Logger::LogLevel::Info, fmt::format("Root exe path '{}'", Filesystem::ExeDir()));
    Logger::log(Logger::LogLevel::Info, fmt::format("Root data path '{}'", Filesystem::DataDir()));

    signal(SIGSEGV, SegFaultHandler); // use handler to print the errors
    signal(SIGINT, SigintHandler);  // Interrupt from keyboard ^C
    signal(SIGQUIT, SigintHandler); // Dump core
    signal(SIGTERM, SigintHandler); // 'termination signal'... ??

    std::thread signal_thread(ThreadCheckShutdown);
    
    Logger::log(Logger::LogLevel::Info, fmt::format("SlugChess Server starting. Version {}", VERSION));
    Run(port, use_tls);
    signal_thread.join();
    Logger::log(Logger::LogLevel::Info, fmt::format("Exiting"));
    return 0;
}


