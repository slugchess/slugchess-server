/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#include "messenger.h"
#include "usermanager.h"

void Messenger::SendServerMessage(const std::string& revicerToken, const std::string& message)
{
    std::string sender =  "server";
    SendMessage(revicerToken, sender, message);
}

void Messenger::SendMessage(std::string& revicerToken, std::string& senderUername, std::string& message)
{
    if(UserManager::Get()->UsertokenLoggedIn(revicerToken)){
        User* user = UserManager::Get()->GetUser(revicerToken);
        std::unique_lock<std::mutex> scopeLock (user->messageStreamMutex);
        chesscom::ChatMessage msg;
        msg.set_allocated_message(&message);
        msg.mutable_user_ident()->set_allocated_usertoken(&revicerToken);
        msg.set_allocated_sender_username(&senderUername);
        user->messageStream->Write(msg); 
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
        msg.release_message();
        msg.release_reciver_usertoken();
        msg.release_sender_username();
#pragma GCC diagnostic pop

    }
    
}

void Messenger::SendMessage(const std::string& revicerToken, const std::string& senderUername, const std::string& message)
{
    if(UserManager::Get()->UsertokenLoggedIn(revicerToken)){
        User* user = UserManager::Get()->GetUser(revicerToken);
        std::unique_lock<std::mutex> scopeLock (user->messageStreamMutex);
        chesscom::ChatMessage msg;
        msg.set_message(message);
        msg.mutable_user_ident()->set_usertoken(revicerToken);
        msg.set_sender_username(senderUername);
        user->messageStream->Write(msg);
    }
    
}
