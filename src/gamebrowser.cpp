/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#include "gamebrowser.h"
#include "matchmanager.h"

GameBrowser* GameBrowser::_instance = 0;

int GameBrowser::HostGame(const chesscom::HostedGame& hostGame, chesscom::LobbyState* lobbyState, std::condition_variable* hostCV, bool* update)
{
    {
        std::unique_lock<std::mutex> scopeLock (_waitingPlayersMutex);
        if(_waitingPlayers.count(hostGame.host().usertoken()) > 0){
            Logger::log(Logger::LogLevel::Error, fmt::format("usertoken: {} tryed to host or join when allready in lobby", hostGame.host().usertoken()));
            throw std::invalid_argument("hosting when allready in lobby");
        }
    }
    int id = GetNewId();
    {
        std::unique_lock<std::mutex> scopeLock1 (_availableGamesMutex);
        std::unique_lock<std::mutex> scopeLock2 (_waitingPlayersMutex);
        _availableGames[id] = hostGame;
        _availableGames[id].set_id(id);
        _waitingPlayers[hostGame.host().usertoken()] = {id, hostCV, lobbyState, update};
        lobbyState->mutable_hosted_game_update()->CopyFrom(_availableGames.at(id));
    }
    Logger::log(Logger::LogLevel::Info, fmt::format("Hosting game id: {} host: {}", id, hostGame.host().usertoken()));
    return id;
}

void GameBrowser::WriteAvailableGames(chesscom::HostedGamesMap& gamesList)
{
    std::unique_lock<std::mutex> scopeLock (_availableGamesMutex);
    *gamesList.mutable_hosted_games() = {_availableGames.begin(), _availableGames.end()};
}

bool GameBrowser::JoinGame(int32_t id, const std::string& usertoken, bool observer, chesscom::LobbyState* lobbyState, std::condition_variable* hostCV, bool* update)
{
    {
        std::unique_lock<std::mutex> scopeLock (_waitingPlayersMutex);
        if(_waitingPlayers.count(usertoken) > 0){
            Logger::log(Logger::LogLevel::Error, fmt::format("usertoken: {} tryed to host or join when allready in lobby", usertoken));
            throw std::invalid_argument("joning when allready in lobby");
        }
    }
    {
        std::unique_lock<std::mutex> scopeLock1 (_waitingPlayersMutex);
        std::unique_lock<std::mutex> scopeLock2 (_availableGamesMutex);
        if(_availableGames.count(id) > 0)
        {
            chesscom::HostedGame& hostedGame = _availableGames.at(id); 
            if(!observer){
                if(hostedGame.has_joiner()){
                    Logger::log(Logger::LogLevel::Info, fmt::format("usertoken: {} joining game {} as opponent when it allready have one. Rejecting join", usertoken, id));
                    return false;
                }
                hostedGame.mutable_joiner()->CopyFrom(*UserManager::Get()->GetUserData(usertoken));
            }else{
                if(!hostedGame.observers_allowed()){
                    Logger::log(Logger::LogLevel::Info, fmt::format("usertoken: {} joining game {} as observer when observing is not allowed. Rejecting join", usertoken, id));
                    return false;
                }
                hostedGame.add_observers()->CopyFrom(*UserManager::Get()->GetUserData(usertoken));
            }
            _waitingPlayers[usertoken] = {id, hostCV, lobbyState, update};
            lobbyState->mutable_hosted_game_update()->CopyFrom(hostedGame);
            AddPlayerChangedToEventQueue(id);
            return true;
        }else{
            Logger::log(Logger::LogLevel::Info, fmt::format("usertoken: {} joining nonexisting game {}. Rejecting join", usertoken, id));
            return false;
        }
    }
}

void GameBrowser::StartGame(int32_t id, const std::string& requester_usertoken)
{
    
    std::unique_lock<std::mutex> scopeLock1 (_waitingPlayersMutex);
    std::unique_lock<std::mutex> scopeLock2 (_availableGamesMutex);
    if(_availableGames.count(id) > 0)
    {
        chesscom::HostedGame& hostedGame = _availableGames.at(id);
        if(hostedGame.has_host() && 
                hostedGame.has_joiner() && 
                hostedGame.host().usertoken() == requester_usertoken){
            //Logger::log(Logger::Debug, "Adding StartGame to eventQueue");
            _eventProcessQueue.push(std::async(std::launch::deferred, [this, id](){               
                chesscom::LobbyState lobbyState;
                std::unique_lock<std::mutex> scopeLock2 (_availableGamesMutex);
                if(_availableGames.count(id) > 0)
                {
                    std::unique_lock<std::mutex> scopeLock1 (_waitingPlayersMutex);
                    chesscom::HostedGame* hostedGame = &_availableGames.at(id);
                    if(hostedGame->has_host() && hostedGame->has_joiner())
                    {
                        std::vector<chesscom::UserData> players;
                        players.assign(hostedGame->observers().begin(), hostedGame->observers().end());
                        players.push_back(hostedGame->host());
                        players.push_back(hostedGame->joiner());

                        std::string matchId = MatchManager::Get()->CreateMatch(*hostedGame);
                        auto mr = lobbyState.mutable_match_result();
                        mr->set_match_token(matchId);
                        mr->mutable_game_rules()->CopyFrom(hostedGame->game_rules());
                        auto match = MatchManager::Get()->GetMatch(matchId);
                        mr->mutable_white_user_data()->CopyFrom(*UserManager::Get()->GetUserData(match->getWhitePlayer()));
                        mr->mutable_black_user_data()->CopyFrom(*UserManager::Get()->GetUserData(match->getBlackPlayer()));
                        mr->mutable_observers()->CopyFrom(hostedGame->observers());
                        _availableGames.erase(id);

                        for (auto&& pdata : players)
                        {
                            //Logger::log(Logger::Debug, fmt::format("StartGame for player {}", pdata.usertoken()));
                            if(_waitingPlayers.count(pdata.usertoken()) > 0){
                                GameBrowser::nl_RemoveWaitingPlayer(pdata.usertoken(), lobbyState);
                            } else {
                                Logger::log(Logger::Warning, fmt::format("StartGame -> player '{}' no longer in _waitingPlayers. Match '{}'", pdata.username(), id));
                            }   
                        }   
                    } else {
                        Logger::log(Logger::Error, fmt::format("StartGameDefered borked as host '{}' or joiner '{}' gone id {}", hostedGame->host().usertoken(), hostedGame->joiner().usertoken(), id));
                        return false;
                    }
                } else {
                    Logger::log(Logger::LogLevel::Warning, fmt::format("match_id: {} StartGameDefered faild to process as game is gone from _availableGames", id));
                    return false;
                }
                Logger::log(Logger::Debug, "StartGame Event completed");
                return true;
            }));
        }
    }
    
}

void GameBrowser::KickPlayer(int32_t id, const std::string& kick_usertoken)
{
    std::unique_lock<std::mutex> scopeLock1 (_waitingPlayersMutex);
    std::unique_lock<std::mutex> scopeLock2 (_availableGamesMutex);
    if(_availableGames.count(id) > 0)
    {
        chesscom::HostedGame& hostedGame = _availableGames.at(id); 
        if(hostedGame.host().usertoken() == kick_usertoken){
            hostedGame.clear_host();
            GameBrowser::nl_RemoveWaitingPlayer(kick_usertoken, chesscom::LobbyState_LobbyClosedReason_KickedFromLobby);
            Logger::log(Logger::LogLevel::Info, fmt::format("usertoken: {} host kicked from game {}", kick_usertoken, id));
            AddCancelHostGameToEventQueue(id);
            return;
        } else if(hostedGame.joiner().usertoken() == kick_usertoken){
            GameBrowser::nl_RemoveWaitingPlayer(kick_usertoken, chesscom::LobbyState_LobbyClosedReason_KickedFromLobby);
            hostedGame.clear_joiner();
            Logger::log(Logger::LogLevel::Info, fmt::format("usertoken: {} joiner kicked from game {}", kick_usertoken, id));
            AddPlayerChangedToEventQueue(id);
            return;
        }
        auto it = std::find_if(hostedGame.observers().begin(), hostedGame.observers().end(), [&kick_usertoken](const auto& x){ return x.usertoken() == kick_usertoken; });
        if(it != hostedGame.observers().end())
        {
            GameBrowser::nl_RemoveWaitingPlayer(kick_usertoken, chesscom::LobbyState_LobbyClosedReason_KickedFromLobby);
            hostedGame.mutable_observers()->erase(it);
            Logger::log(Logger::LogLevel::Info, fmt::format("usertoken: {} observer kicked from game{}", kick_usertoken, id));
            AddPlayerChangedToEventQueue(id);
            return;
        }
    }else{
        Logger::log(Logger::LogLevel::Info, fmt::format("usertoken: {} leaving nonexisting game {}.", kick_usertoken, id));
    }
}

void GameBrowser::nl_RemoveWaitingPlayer(const std::string& usertoken, chesscom::LobbyState_LobbyClosedReason removed_reason)
{
    if(_waitingPlayers.count(usertoken) < 1){
        Logger::log(Logger::LogLevel::Debug, fmt::format("usertoken: {} nl_RemoveWaitingPlayer but player not in _waitingPlayers", usertoken));
        return;
    }
    WaitingPlayer* player = &_waitingPlayers.at(usertoken);
    player->lobbyState->set_lobby_closed(removed_reason);//chesscom::LobbyState_LobbyClosedReason_KickedFromLobby);
    *player->update = true;
    player->cv->notify_all();
    _waitingPlayers.erase(usertoken);
}

void GameBrowser::nl_RemoveWaitingPlayer(const std::string& usertoken, const chesscom::LobbyState& lobby_state)
{
    if(_waitingPlayers.count(usertoken) < 1){
        Logger::log(Logger::LogLevel::Debug, fmt::format("usertoken: {} nl_RemoveWaitingPlayer but player not in _waitingPlayers", usertoken));
        return;
    }
    WaitingPlayer* player = &_waitingPlayers.at(usertoken);
    player->lobbyState->CopyFrom(lobby_state);
    *player->update = true;
    player->cv->notify_all();
    _waitingPlayers.erase(usertoken);
}

void GameBrowser::AddPlayerChangedToEventQueue(int32_t id)
{
    //Logger::log(Logger::Warning, fmt::format("Pushing to event queue"));
    _eventProcessQueue.push(std::async(std::launch::deferred, [this, id](){
        
        //Bind to user
        WaitingPlayer* player; 

        chesscom::HostedGame* hostedGame;
        chesscom::LobbyState lobbyState;
        std::unique_lock<std::mutex> scopeLock2 (_availableGamesMutex);
        if(_availableGames.count(id) > 0)
        {
            std::unique_lock<std::mutex> scopeLock1 (_waitingPlayersMutex);
            hostedGame = &_availableGames.at(id);
            std::vector<chesscom::UserData> players;
            players.assign(hostedGame->observers().begin(), hostedGame->observers().end());
            if(hostedGame->has_host())players.push_back(hostedGame->host());
            if(hostedGame->has_joiner()){
                players.push_back(hostedGame->joiner());
            }
            lobbyState.mutable_hosted_game_update()->CopyFrom(*hostedGame);
            
            for (auto&& pdata : players)
            {
                //Logger::log(Logger::Debug, fmt::format("AddPlayerChangedToEvent for player {}", pdata.usertoken()));
                if(_waitingPlayers.count(pdata.usertoken()) > 0){
                    player = &_waitingPlayers.at(pdata.usertoken());
                    player->lobbyState->CopyFrom(lobbyState);
                    *player->update = true;
                    player->cv->notify_all();
                } else {
                    Logger::log(Logger::Warning, fmt::format("AddPlayerChangedToEvent -> player '{}' no longer in _waitingPlayers. Match '{}'", pdata.username(), id));
                }
                
            }   
        }else{
            Logger::log(Logger::LogLevel::Warning, fmt::format("match_id: {} faild to process as game is gone from _availableGames", id));
            return false;
        }
        //Logger::log(Logger::Debug, "AddPlayerChangedToEvent completed");
        return true;
    }));
}

void GameBrowser::LeaveGame(int id, const std::string& usertoken)
{    
    std::unique_lock<std::mutex> scopeLock1 (_waitingPlayersMutex);
    std::unique_lock<std::mutex> scopeLock2 (_availableGamesMutex);
    if(_availableGames.count(id) > 0)
    {
        chesscom::HostedGame& hostedGame = _availableGames.at(id); 
        if(hostedGame.host().usertoken() == usertoken){
            _waitingPlayers.erase(usertoken);
            hostedGame.clear_host();
            Logger::log(Logger::LogLevel::Info, fmt::format("usertoken: {} as host left game game {}", usertoken, id));
            AddCancelHostGameToEventQueue(id);
            return;
        } else if(hostedGame.joiner().usertoken() == usertoken){
            _waitingPlayers.erase(usertoken);
            hostedGame.clear_joiner();
            Logger::log(Logger::LogLevel::Info, fmt::format("usertoken: {} as joiner left game game {}", usertoken, id));
            AddPlayerChangedToEventQueue(id);
            return;
        }
        auto it = std::find_if(hostedGame.observers().begin(), hostedGame.observers().end(), [&usertoken](const auto& x){ return x.usertoken() == usertoken; });
        if(it != hostedGame.observers().end())
        {
            _waitingPlayers.erase(usertoken);
            hostedGame.mutable_observers()->erase(it);
            Logger::log(Logger::LogLevel::Info, fmt::format("usertoken: {} as observer left game game {}", usertoken, id));
            AddPlayerChangedToEventQueue(id);
            return;
        }
    }else{
        Logger::log(Logger::LogLevel::Info, fmt::format("usertoken: {} leaving nonexisting game {}.", usertoken, id));
    }
}

void GameBrowser::AddCancelHostGameToEventQueue(int id)
{
    _eventProcessQueue.push(std::async(std::launch::deferred, [this, id](){
        
        std::unique_lock<std::mutex> scopeLock1 (_waitingPlayersMutex);
        std::unique_lock<std::mutex> scopeLock2 (_availableGamesMutex);

        if(_availableGames.count(id) > 0)
        {
            auto& hostedGame = _availableGames.at(id);

            chesscom::LobbyState lobbyState;
            lobbyState.set_lobby_closed(chesscom::LobbyState_LobbyClosedReason_HostLeft);
            std::vector<chesscom::UserData> players;
            players.assign(hostedGame.observers().begin(), hostedGame.observers().end());
            if(hostedGame.has_host()) players.push_back(hostedGame.host());
            if(hostedGame.has_joiner()) players.push_back(hostedGame.joiner());
            for (auto&& pdata : players)
            {
                GameBrowser::nl_RemoveWaitingPlayer(pdata.usertoken(), lobbyState);
            }   
            
            _availableGames.erase(id);
            Logger::log(Logger::Debug, "CancelHostGameEvent completed");
            return true;
        }
        Logger::log(Logger::Warning, fmt::format("Game {} gone when closing", id));
        return false;
    }));
}

void GameBrowser::UserLoggedOut(const std::string& usertoken, std::shared_ptr<chesscom::UserData> userData)
{   
    if(_waitingPlayers.count(usertoken) > 0)
    {
        std::unique_lock<std::mutex> scopeLock2 (_availableGamesMutex);
        std::unique_lock<std::mutex> scopeLock1 (_waitingPlayersMutex);
        auto& player = _waitingPlayers.at(usertoken);
        if(_availableGames.count(player.gameId) > 0){
            auto& hostedGame = _availableGames.at(player.gameId);
            if(hostedGame.host().usertoken() == usertoken)
            {
                _eventProcessQueue.push(std::async(std::launch::deferred, 
                    [this, id = player.gameId, usertoken](){ 
                        LeaveGame(id, usertoken); 
                        return true;
                    }));
                return;
            }
            if(hostedGame.joiner().usertoken() == usertoken)
            {
                _waitingPlayers.erase(usertoken);
                hostedGame.clear_joiner();
                Logger::log(Logger::LogLevel::Info, fmt::format("usertoken: {} as joiner left game by logging out game {}", usertoken, player.gameId));
                AddPlayerChangedToEventQueue(player.gameId);
                return;
            }
            auto it = std::find_if(hostedGame.observers().begin(), hostedGame.observers().end(), [&usertoken](const auto& x){ return x.usertoken() == usertoken; });
            if(it != hostedGame.observers().end())
            {
                _waitingPlayers.erase(usertoken);
                hostedGame.mutable_observers()->erase(it);
                Logger::log(Logger::LogLevel::Info, fmt::format("usertoken: {} as observer left game by logging out game {}", usertoken, player.gameId));
                AddPlayerChangedToEventQueue(player.gameId);
                return;
            }
            Logger::log(Logger::LogLevel::Warning, fmt::format("usertoken: {} in _waitingPlayers but not in a game", usertoken));
            _waitingPlayers.erase(usertoken);
        }
    }
}

void GameBrowser::DoWork()
{
    while (!_eventProcessQueue.empty())
    {
        auto& event = _eventProcessQueue.front();
        if(!event.get())
        {
            Logger::log(Logger::Warning, "A gamebrowser event failed");
        }
        //Logger::log(Logger::Debug, "Poping a gamebrowser event");
        _eventProcessQueue.pop();
    }
}

void GameBrowser::UpdateLobby(const std::string& usertoken){
        
    if(_waitingPlayers.count(usertoken) > 0)
    {
        std::unique_lock<std::mutex> scopeLock2 (_availableGamesMutex);
        std::unique_lock<std::mutex> scopeLock1 (_waitingPlayersMutex);
        auto& player = _waitingPlayers.at(usertoken);
        if(_availableGames.count(player.gameId) > 0){
            
            auto& hostedGame = _availableGames.at(player.gameId);
            std::vector<chesscom::UserData> players;
            players.assign(hostedGame.observers().begin(), hostedGame.observers().end());
            if(hostedGame.has_host())players.push_back(hostedGame.host());
            if(hostedGame.has_joiner())players.push_back(hostedGame.joiner());
            
            chesscom::LobbyState lobbyState;
            lobbyState.mutable_hosted_game_update()->CopyFrom(hostedGame);
            
            Logger::log(Logger::Debug, fmt::format("UpdateLobby for player {}", usertoken));
            auto& player = _waitingPlayers.at(usertoken);
            player.lobbyState->CopyFrom(lobbyState);
            *player.update = true;
            player.cv->notify_all();
        
        }
    }
}



std::pair<chesscom::LobbyCommandResultType, int32_t> GameBrowser::IsHostAndWhatHostedGame(const std::string& usertoken)
{
    if(_waitingPlayers.count(usertoken) > 0)
    {
        std::unique_lock<std::mutex> scopeLock2 (_availableGamesMutex);
        std::unique_lock<std::mutex> scopeLock1 (_waitingPlayersMutex);
        auto& player = _waitingPlayers.at(usertoken);
        if(_availableGames.count(player.gameId) > 0){
            auto& hostedGame = _availableGames.at(player.gameId);
            if(hostedGame.host().usertoken() == usertoken){
                return {chesscom::LOBBY_COMMAND_RESULT_TYPE_SUCCESS, player.gameId};    
            }
            return {chesscom::LOBBY_COMMAND_RESULT_TYPE_NOT_HOST, -1};    
        }
    }
    return {chesscom::LOBBY_COMMAND_RESULT_TYPE_NOT_IN_LOBBY, -1};
}

chesscom::LobbyCommandResultType GameBrowser::LobbyCommandStartGame(const std::string& usertoken)
{
    auto [result, gameId] = IsHostAndWhatHostedGame(usertoken);
    if(result == chesscom::LOBBY_COMMAND_RESULT_TYPE_SUCCESS){
        StartGame(gameId, usertoken);
        DoWork(); // Do some work before you leave. You added an event after all
    }
    return result;
}
chesscom::LobbyCommandResultType GameBrowser::LobbyCommandKickPlayer(const std::string& requester_usertoken, const std::string& kick_usertoken)
{
    auto [result, gameId] = IsHostAndWhatHostedGame(requester_usertoken);
    if(result == chesscom::LOBBY_COMMAND_RESULT_TYPE_SUCCESS){
        KickPlayer(gameId, kick_usertoken);
        DoWork(); // Do some work before you leave. You added an event after all
    }
    return result;
}