/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include <list>
#include <map>
#include <functional>
#include <thread>
#include <mutex>
#include <chrono>

#include "logger.h"

struct Work
{
    std::string id;
    std::chrono::seconds interval;
    std::function<void()> func;

    Work(const std::string& id, std::chrono::seconds interval, std::function<void()> func)
    {
        this->id = id; this->interval = interval; this->func = func;
    }

    bool operator==(const Work& work)
    {
        return id == work.id;
    }
    
};

class Worker
{
    private:
    static Worker* _instance;
    #define WORK_INTERVAL_MILI 1000

    //MAYBEDO add condition_variable to wake of thread if more work is needed
    
    std::mutex _workLock;
    //std::map<std::string, std::function<void()>> _workFunctions;
    std::thread _worker;
    bool _stop_worker = false;

    std::list<std::shared_ptr<Work>> _workList;
    std::map<std::string, std::chrono::time_point<std::chrono::system_clock>> _lastTimeWorkedOn;

    std::mutex _changeLock;
    std::list<std::shared_ptr<Work>> _addWorkList;
    //std::map<std::string, std::function<void()>> _removeFunctions;

    Worker() {}
    void WorkLoop();
    bool NeedIteration();
    void w_AddRemoveFunctions();
    public:
    static void Start();
    static void Stop();
    static void Join();
    //static void ClearWork();
    static bool AddWork(std::shared_ptr<Work>);
    //static void RemoveWork(std::function<void()> func);


};