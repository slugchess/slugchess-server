/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#include "chesscomservice.h"
#include <fmt/chrono.h>
ChessComService::ChessComService() 
{
    tokenCounter = 1;
}
ChessComService::~ChessComService()
{
    Logger::log(Logger::LogLevel::Info, "ChessComService Destroyed");
}
grpc::Status ChessComService::RegisterUser(ServerContext* context, const chesscom::RegiserUserForm* request, chesscom::RegiserUserFormResult* response) 
{
    if(request->username().find('/') != std::string::npos){
        response->set_success(false);
        response->set_fail_message("Username '" + request->username() + "' contains '/'. Choose another one.");
        return Status::OK;
    }
    if(request->username().size() > 80){
        response->set_success(false);
        response->set_fail_message("Username '" + request->username() + "' is to long. Choose another one.");
        return Status::OK;
    }
    if(request->password().size() > 80){
        response->set_success(false);
        response->set_fail_message("Password is to long. Choose another one.");
        return Status::OK;
    }
    if(Filesystem::UserFileExists(UserStore::UsernameToDataFilename(request->username()))){
        response->set_success(false);
        response->set_fail_message("Username '" + request->username() + "' already exists. Choose another one.");
        return Status::OK;
    }else{
        auto encry_key = UserStore::EncryptionKeyFromPassword(request->password());
        chesscom::UserStaticData ud;
        ud.set_username(request->username());
        ud.set_elo(1500);
         try {
            std::string encrypted = UserStore::encrypt_string(SECRET, encry_key);
            ud.set_secret(encrypted);
       
            Filesystem::WriteUserFile(ud, UserStore::UsernameToDataFilename(ud.username()));
            response->set_success(true);
            return Status::OK;
        }
        catch (std::invalid_argument& ex){
            
            Logger::log(Logger::LogLevel::Warning, "Failed to create user:" + std::string(ex.what()));
            //think about delete user data incase it got half writen
            response->set_success(false);
            response->set_fail_message("Something whent wring when create user. Please try again with different username or password");
            return Status::OK;
        }
    }
    return Status::CANCELLED;

}

Status ChessComService::Login(ServerContext* context, const chesscom::LoginForm* request, chesscom::LoginResult* response) 
{
    //START BY VALIDATING USER
    const auto encryption_key = UserStore::EncryptionKeyFromPassword(request->password());
    const auto filename = UserStore::UsernameToDataFilename(request->username());
    if(!Filesystem::UserFileExists(filename)){
        response->set_successfull_login(false);
        response->set_login_message("Username or password is wrong");
        return Status::OK;
    }
    auto& userData = *response->mutable_user_data();
    try{
        auto userdata_static = Filesystem::ReadUserFile(UserStore::UsernameToDataFilename(request->username()));
        if(SECRET == UserStore::decrypt_string(userdata_static.secret(), encryption_key)){
            userData.set_username(userdata_static.username());
            userData.set_elo(userdata_static.elo());
        }else{
            response->set_successfull_login(false);
            response->set_login_message("Username or password is wrong");
            return Status::OK;
        }

    } catch(std::invalid_argument& ex){
        response->set_successfull_login(false);
        response->set_login_message("Username and password correct, but loading of userdata failed. If trying again dossend wourk you are fucked");
        return Status::OK;
    }
    
    
    if(request->major_version() == MAJOR_VER && request->minor_version() == MINOR_VER)
    {
        Logger::log(Logger::LogLevel::Info, fmt::format("User {} {} {} {} {} {}", request->major_version(), MAJOR_VER, request->minor_version(), MINOR_VER, request->build_version(), BUILD_VER));
        if(request->build_version() >= BUILD_VER)
        {
            
        }
        else
        {
            response->set_login_message("You should upgrade to latest version. Server version " VERSION ", Client version " + request->major_version() + "." + request->minor_version() + "." + request->build_version()+". You can find the latest version at 'http://spaceslug.no/slugchess/'");
        }
        std::string userToken = request->username() + "-" + std::to_string(tokenCounter++) + "-" + generate_random_alphanum(6);
        userData.set_usertoken(userToken);
        auto secret = UserManager::Get()->LogInUser(userToken, userData, encryption_key);
        Logger::log(Logger::LogLevel::Info, fmt::format("User {} {} logged in", response->user_data().username(), response->user_data().usertoken()));
        response->set_secret(secret);
        response->set_successfull_login(true);
    }
    else if(request->major_version() < MAJOR_VER || (request->major_version() == MAJOR_VER && request->minor_version() < MINOR_VER))
    {
        response->set_successfull_login(false);
        response->set_login_message("Login failed because version missmatch. Server version " VERSION ", Client version " + request->major_version() + "." + request->minor_version() + "." + request->build_version()+". Normaly SlugChess Client should autoupdate. You can find the latest version at 'https://spaceslug.no/slugchess/'");
    }
    else if(request->major_version() > MAJOR_VER || (request->major_version() == MAJOR_VER && request->minor_version() > MINOR_VER))
    {
        response->set_login_message("Client version higher then server. Assuming you are dev or something. Server version " VERSION ", Client version " + request->major_version() + "." + request->minor_version() + "." + request->build_version()+".");
        std::string userToken = request->username() + "-" + std::to_string(tokenCounter++);
        userData.set_usertoken(userToken);
        auto secret = UserManager::Get()->LogInUser(userToken, userData, encryption_key);
        response->set_secret(secret);
        response->set_successfull_login(true);
    }
    else
    {
        response->set_successfull_login(false);
        response->set_login_message("Login failed because unhandled version case. Server version " VERSION ", Client version " + request->major_version() + "." + request->minor_version() + "." + request->build_version());
    }
    return Status::OK;
}

grpc::Status ChessComService::GetNamedVariants(ServerContext* context, const chesscom::Void* request, chesscom::NamedVariants* response) 
{
    //response->set_variants();
    auto variants = SlugChess::GetVariants();
    *response->mutable_variants() = {variants.begin(), variants.end()};
    return Status::OK;
}

grpc::Status ChessComService::ServerVisionRulesets(grpc::ServerContext* context, const chesscom::Void *request, chesscom::VisionRuleset* response) 
{
    auto visionRules = SlugChess::GetVisionRules();
    for(auto& [name, scRule] : visionRules){
        response->mutable_vision_rulesets()->insert({name, MatchManager::Get()->FromSlugChessVisionRules(*scRule)}); 
    }
    return grpc::Status::OK;
}
    
grpc::Status ChessComService::Logout(grpc::ServerContext* context, const chesscom::UserIdentification* request, chesscom::LogoutResult* response) 
{
    if(!UserManager::Get()->Authenticate(*request)){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    response->set_successfull_logout(UserManager::Get()->Logout(request->usertoken()));
    if(!response->successfull_logout()){
        response->set_logout_message("Could not logout as user not logged in");
    }
    return Status::OK;
}

Status ChessComService::LookForMatch(ServerContext* context, const chesscom::UserIdentification* request, chesscom::LookForMatchResult* response) 
{
    if(!UserManager::Get()->Authenticate(*request)){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    return grpc::Status(grpc::StatusCode::DO_NOT_USE, "Currently not in use");
    //return Status::OK;
}

void ChessComService::MatchReadLoop(ServerContext* context, std::shared_ptr<::Match> matchPtr, grpc::ServerReaderWriter< chesscom::MoveResult, chesscom::MovePacket>* stream){
    chesscom::MovePacket movePkt;
    bool keepRunning = true;
    try{
        while (!context->IsCancelled() && keepRunning)
        {
            if(!stream->Read(&movePkt)){
                //throw "premeture end of steam"
                Logger::log(Logger::LogLevel::Info, fmt::format("{} {}  MatchStreamCanceled in read thread", movePkt.match_token(), movePkt.user_ident().usertoken()));
                break;
            }
            time_point move_received = timeNow();
            if(movePkt.asking_for_draw())throw "draw not implemented";
            std::shared_ptr<chesscom::Move> movePtr = std::make_shared<chesscom::Move>(movePkt.move());
            // if(movePkt.doing_move()){
            //     movePtr->set_from(movePkt.move().from());
            //     movePtr->set_to(movePkt.move().to());
            //     movePtr->mutable_timestamp()->Swap(movePkt.mutable_move()->mutable_timestamp());
            //     movePtr->set_sec_spent(movePkt.move().sec_spent());
            // }
            MatchManager::DoMoveInMatch(movePkt.cheat_matchevent(), movePkt.user_ident().usertoken(), matchPtr, movePtr, move_received);
        }
        Logger::log(Logger::LogLevel::Info, fmt::format("{} {} ReadThread End", movePkt.match_token(), movePkt.user_ident().usertoken()));
    }
    catch(std::exception& ex)
    {
        Logger::log(Logger::LogLevel::Info, fmt::format("{} {} Gracefully exit read thread exception: {}", movePkt.match_token(), movePkt.user_ident().usertoken(), ex.what()));
    }
}

Status ChessComService::Match(ServerContext* context, grpc::ServerReaderWriter< chesscom::MoveResult, chesscom::MovePacket>* stream) 
{
    
    chesscom::MovePacket movePkt;
    stream->Read(&movePkt);
    if(!UserManager::Get()->Authenticate(movePkt.user_ident())){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    if(movePkt.doing_move()){
        Logger::log(Logger::LogLevel::Warning, fmt::format("doing move as first {}. Ignoreing!", movePkt.user_ident().usertoken()));
        return Status::OK; 
    }
    Logger::log(Logger::LogLevel::Info, fmt::format("Opening matchstream for {} {}", movePkt.match_token(), movePkt.user_ident().usertoken()));
    std::string userToken = movePkt.user_ident().usertoken();
    std::shared_ptr<::Match> matchPtr = MatchManager::Get()->GetMatch(movePkt.match_token());
    if(matchPtr == nullptr){
        Logger::log(Logger::LogLevel::Info, fmt::format("{} Falied to retreve matchPtr ", movePkt.match_token(), movePkt.user_ident().usertoken()));    
    }
    Logger::log(Logger::LogLevel::Info, fmt::format("{} {} Starting read thread ", movePkt.match_token(), movePkt.user_ident().usertoken()));
    std::thread t1([this, context, matchPtr, stream](){
        this->MatchReadLoop(context, matchPtr, stream);
    });
    //Blocks until match is over
    MatchManager::MatchListenLoop(userToken, matchPtr, context, stream);
    if(context->IsCancelled()) {
        Logger::log(Logger::LogLevel::Debug, fmt::format("{} {} Matchstream cancelled ", movePkt.match_token(), movePkt.user_ident().usertoken()));
        t1.join();
        return grpc::Status::OK;
    }
    t1.join();
    MatchManager::Get()->EraseMatch(movePkt.match_token());
    Logger::log(Logger::LogLevel::Debug, fmt::format("{} {} Matchstream ended.", movePkt.match_token(), movePkt.user_ident().usertoken()));
    return Status::OK;
}

grpc::Status ChessComService::MatchEventListener(grpc::ServerContext *context, const chesscom::MatchObserver* request, grpc::ServerWriter<chesscom::MoveResult> *writer)
{
    if(!UserManager::Get()->Authenticate(request->user_ident())){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    Logger::log(Logger::LogLevel::Debug, fmt::format("Opening MatchEventListener for {} {}", request->match_id(), request->user_ident().usertoken()));
    std::shared_ptr<::Match> matchPtr = MatchManager::Get()->GetMatch(request->match_id());
    //Blocks until match is over
    MatchManager::MatchListenLoop(request->user_ident().usertoken(), matchPtr, context, writer);
    if(context->IsCancelled()) {
        Logger::log(Logger::LogLevel::Debug, fmt::format("{} {} MatchEventListener cancelled ", request->match_id(), request->user_ident().usertoken()));
        return grpc::Status::OK;
    }
    MatchManager::Get()->EraseMatch(request->match_id());
    Logger::log(Logger::LogLevel::Info, fmt::format("{} {} MatchEventListener ended.", request->match_id(), request->user_ident().usertoken()));
    return Status::OK;
}

grpc::Status ChessComService::SendMove(grpc::ServerContext* context, const chesscom::MovePacket* request, chesscom::Void* response) 
{
    if(!UserManager::Get()->Authenticate(request->user_ident())){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    time_point move_received = timeNow();
    if(request->user_ident().usertoken() == ""){
        Logger::log(Logger::LogLevel::Info, fmt::format("Got SendMove call with invalid usertoken {}", request->user_ident().usertoken()));
        return grpc::Status::CANCELLED;
    }
    if(request->match_token() == ""){
        Logger::log(Logger::LogLevel::Info, fmt::format("Got SendMove call with invalid match_token{}", request->user_ident().usertoken()));
        return grpc::Status::CANCELLED;
    }
    if(request->asking_for_draw())throw "draw not implemented";
    std::shared_ptr<::Match> matchPtr = MatchManager::Get()->GetMatch(request->match_token());
    if(matchPtr.get() == nullptr){
        Logger::log(Logger::LogLevel::Info, fmt::format("Got SendMove call with match_token but no match of that token was found {}", request->user_ident().usertoken()));
        return grpc::Status::CANCELLED;
    }
    std::shared_ptr<chesscom::Move> movePtr = std::make_shared<chesscom::Move>(request->move());
    MatchManager::DoMoveInMatch(request->cheat_matchevent(), request->user_ident().usertoken(), matchPtr, movePtr, move_received);
    
    return grpc::Status::OK;
}

void ChessComService::ChatMessageStreamLoop(ServerContext* context, std::string& usertoken, grpc::ServerReaderWriter< chesscom::ChatMessage, chesscom::ChatMessage>* stream){
    chesscom::ChatMessage chatPkt;
    bool keepRunning = true;
    try{
        while (!context->IsCancelled() && keepRunning)
        {
            if(!stream->Read(&chatPkt)){
                //throw "premeture end of steam"
                Logger::log(Logger::LogLevel::Warning, fmt::format("{} ChatStreamCanceled in read thread ", usertoken));
                break;
            }
            if(chatPkt.user_ident().usertoken() != usertoken)
            {
                Logger::log(Logger::LogLevel::Warning, fmt::format("{} Wrong usertoken in chat thread ", usertoken));
            }
            else
            {
                Logger::log(Logger::LogLevel::Info, fmt::format("{} Prossesing massage to {}", usertoken, chatPkt.reciver_usertoken()));
                Messenger::SendMessage(*chatPkt.mutable_reciver_usertoken(), *chatPkt.mutable_sender_username(), *chatPkt.mutable_message());                
            }
            
        }
        Logger::log(Logger::LogLevel::Info, fmt::format("{} ChatMessageStream End", usertoken));
    }
    catch(std::exception& ex)
    {
        Logger::log(Logger::LogLevel::Warning, fmt::format("{} Gracefully exit ChatMessageStream thread exception: {}", usertoken, ex.what()));
    }
}

Status ChessComService::ChatMessageStream(ServerContext* context, grpc::ServerReaderWriter< chesscom::ChatMessage, chesscom::ChatMessage>* stream) 
{
    chesscom::ChatMessage chatPkt;
    stream->Read(&chatPkt);
    if(!UserManager::Get()->Authenticate(chatPkt.user_ident())){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    std::string usertoken = chatPkt.user_ident().usertoken();
    Logger::log(Logger::LogLevel::Debug, fmt::format("Opening ChatMessageStream for {}", usertoken));
    std::thread t1([this, context, &usertoken, stream](){
        this->ChatMessageStreamLoop(context, usertoken, stream);
    });
    UserManager::Get()->AddMessageStream(usertoken, &(*stream));
    bool loop = true;
    while (loop)
    {
        if(context->IsCancelled()) {
            Logger::log(Logger::LogLevel::Debug, fmt::format("{} ChatMessageStream cancelled ", usertoken));
            t1.join();
            UserManager::Get()->RemoveMessageStream(usertoken);
            return grpc::Status::OK;
        }
        
        std::this_thread::sleep_for(std::chrono::milliseconds(MAX_SLEEP_MS));
    }
    t1.join();
    UserManager::Get()->RemoveMessageStream(usertoken);
    Logger::log(Logger::LogLevel::Debug, fmt::format("{} ChatMessageStream ended.", usertoken));
    return Status::OK;
}

grpc::Status ChessComService::HostGame(grpc::ServerContext *context, const chesscom::HostGameRequest *request, grpc::ServerWriter<chesscom::LobbyState> *writer)
{
    if(!UserManager::Get()->Authenticate(request->user_ident())){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    if(!UserManager::Get()->UsertokenLoggedIn(request->user_ident().usertoken())) return grpc::Status::CANCELLED;
    //Logger::log(Logger::LogLevel::Info, fmt::format(" hosting game enter:"));
    chesscom::HostedGame hostedGame;
    hostedGame.mutable_game_rules()->CopyFrom(request->hosted_game().game_rules());
    hostedGame.mutable_host()->CopyFrom(*UserManager::Get()->GetUserData(request->user_ident().usertoken()));
    hostedGame.set_observers_allowed(request->hosted_game().observers_allowed());
    std::mutex mutex;
    std::condition_variable cv;
    std::unique_lock<std::mutex> lk(mutex);
    bool update = false;
    chesscom::LobbyState lobbyState;
    int id = GameBrowser::Get()->HostGame(hostedGame, &lobbyState, &cv, &update);
    {
        chesscom::LobbyState lobbyStateTemp = lobbyState;
        writer->Write(lobbyStateTemp);
    }
    while (!context->IsCancelled())
    {
        if(update){
            switch (lobbyState.event_case())
            {
            case chesscom::LobbyState::EventCase::kMatchResult:
                writer->Write(lobbyState);
                return grpc::Status::OK;
                break;
            case chesscom::LobbyState::EventCase::kLobbyClosed:
                writer->Write(lobbyState);
                return grpc::Status::OK;
                break;
            case chesscom::LobbyState::EventCase::kHostedGameUpdate:
                writer->Write(lobbyState);
                break;
            
            default:
                break;
            }
        }
        cv.wait_for(lk, std::chrono::milliseconds(MAX_SLEEP_MS));
    }
    Logger::log(Logger::LogLevel::Debug, fmt::format("Stopped hosting game id:{}, contexed canceled? {}", id, context->IsCancelled()));
    GameBrowser::Get()->LeaveGame(id, request->user_ident().usertoken());
    return grpc::Status::OK;
}

grpc::Status ChessComService::AvailableGames(grpc::ServerContext *context, const chesscom::UserIdentification *request, chesscom::HostedGamesMap *response) 
{
    if(!UserManager::Get()->Authenticate(*request)){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    GameBrowser::Get()->WriteAvailableGames(*response);
    return grpc::Status::OK;
}

grpc::Status ChessComService::JoinGame(grpc::ServerContext *context, const chesscom::JoinGameRequest *request, grpc::ServerWriter<chesscom::LobbyState> *writer) 
{
    if(!UserManager::Get()->Authenticate(request->user_ident())){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    std::mutex mutex;
    std::condition_variable cv;
    std::unique_lock<std::mutex> lk(mutex);
    bool update = false;
    chesscom::LobbyState lobbyState;
    if(GameBrowser::Get()->JoinGame(request->id(), request->user_ident().usertoken(), request->observer(), &lobbyState, &cv, &update))
    {
        chesscom::LobbyState lobbyStateTemp = lobbyState;
        writer->Write(lobbyStateTemp);
        while (!context->IsCancelled())
        {
            if(update){
                switch (lobbyState.event_case())
                {
                case chesscom::LobbyState::EventCase::kMatchResult:
                    writer->Write(lobbyState);
                    return grpc::Status::OK;
                    break;
                case chesscom::LobbyState::EventCase::kLobbyClosed:
                    writer->Write(lobbyState);
                    return grpc::Status::OK;
                    break;
                case chesscom::LobbyState::EventCase::kHostedGameUpdate:
                    writer->Write(lobbyState);
                    break;
                
                default:
                    break;
                }
            }
            cv.wait_for(lk, std::chrono::milliseconds(MAX_SLEEP_MS));
        }
    }else{
        Logger::log(Logger::LogLevel::Warning, fmt::format("Failed to join game id:{}", request->id()));
        return {grpc::StatusCode::PERMISSION_DENIED, "Jointing game failed"};
    }
    
    Logger::log(Logger::LogLevel::Debug, fmt::format("left lobby game id:{}", request->id()));
    GameBrowser::Get()->LeaveGame(request->id(), request->user_ident().usertoken());
    return grpc::Status::OK;


    return grpc::Status::OK;
}

grpc::Status ChessComService::LobbyCommand(grpc::ServerContext *context, const chesscom::LobbyCommandMessage *request, chesscom::LobbyCommandResult *response) 
{
    if(!UserManager::Get()->Authenticate(request->user_ident())){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    if(request->cmd_case() == chesscom::LobbyCommandMessage::CmdCase::kStartGame){

        response->set_result(GameBrowser::Get()->LobbyCommandStartGame(request->user_ident().usertoken()));
    }else if(request->cmd_case() == chesscom::LobbyCommandMessage::CmdCase::kKickPlayerUsertoken){
        response->set_result(GameBrowser::Get()->LobbyCommandKickPlayer(request->user_ident().usertoken(), request->kick_player_usertoken()));
    }
    if(!response->result() == chesscom::LOBBY_COMMAND_RESULT_TYPE_SUCCESS){
        GameBrowser::Get()->UpdateLobby(request->user_ident().usertoken());
    }

    return grpc::Status::OK;
}

grpc::Status ChessComService::Alive(grpc::ServerContext* context, const chesscom::Heartbeat* request, chesscom::Heartbeat* response)
{
    if(!UserManager::Get()->Authenticate(request->user_ident())){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    if(!UserManager::Get()->UsertokenLoggedIn(request->user_ident().usertoken()))
    {
        Logger::log(Logger::LogLevel::Info, fmt::format("'{}' is not a logged in user. Heartbeat failed", request->user_ident().usertoken()));       
        response->set_alive(false);
        response->mutable_user_ident()->set_usertoken(request->user_ident().usertoken());
        return grpc::Status::OK;
    }
    if(UserManager::Get()->Heartbeat(request->user_ident().usertoken()))
    {
        response->set_alive(true);
        response->mutable_user_ident()->set_usertoken(request->user_ident().usertoken());
        return grpc::Status::OK;
    }
    else
    {
        Logger::log(Logger::LogLevel::Info, fmt::format("'{}' failed heartbeat test and was force logged out", request->user_ident().usertoken()));
        UserManager::Get()->Logout(request->user_ident().usertoken());
        response->set_alive(false);
        response->mutable_user_ident()->set_usertoken(request->user_ident().usertoken());
        return grpc::Status::OK;
    }

}

grpc::Status ChessComService::ChatMessageListener(grpc::ServerContext* context, const chesscom::UserIdentification* request, grpc::ServerWriter< ::chesscom::ChatMessage>* writer) 
{
    if(!UserManager::Get()->Authenticate(*request)){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    //Logger::log(Logger::LogLevel::Info, fmt::format("Opening ChatMessageStream for {}", request->usertoken()));
    UserManager::Get()->AddMessageStream(request->usertoken(), writer);
    std::condition_variable& changedCV =  UserManager::Get()->GetUser(request->usertoken())->changedCV;
    std::mutex mutex;
    std::unique_lock<std::mutex> lk(mutex);
    Messenger::SendMessage(request->usertoken(), "system", "Welcome " + UserManager::Get()->GetUser(request->usertoken())->data->username() + " to SlugChess. You are now connected to the chat system.");
    // TODO: enable when commands is implemented
    //Messenger::SendMessage(request->usertoken(), "system", "Type '/help' from more info on commands");
    while (true)
    {
        if(context->IsCancelled()) {
            //Logger::log(Logger::LogLevel::Info, fmt::format("{} ChatMessageStream cancelled ", request->usertoken()));
            UserManager::Get()->RemoveMessageStream(request->usertoken());
            return grpc::Status::OK;
        }
        if(!UserManager::Get()->UsertokenLoggedIn(request->usertoken()))
        {
            //Logger::log(Logger::LogLevel::Info, "ChatMessage stream ended because " + request->usertoken() + " logged out");
            return grpc::Status::OK;
        }
        changedCV.wait_for(lk, std::chrono::milliseconds(MAX_SLEEP_MS));
//        std::this_thread::sleep_for(std::chrono::milliseconds(MAX_SLEEP_MS));
    }
    return grpc::Status::OK;
}

grpc::Status ChessComService::SendChatMessage(grpc::ServerContext* context, const chesscom::ChatMessage* request, chesscom::Void* response)
{
    if(!UserManager::Get()->Authenticate(request->user_ident())){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    //Also sending the message back to sender so it shows up in their log
    //  no Their problem
    // TODO: make messageing accually make sense
    Messenger::SendMessage(request->reciver_usertoken(), request->sender_username(), request->message());
    //Messenger::SendMessage(request->user_ident().usertoken(), request->sender_username(), request->message());
    return grpc::Status::OK;
}

grpc::Status ChessComService::ProcessReplay(grpc::ServerContext* context,  const chesscom::ReplayRequest* request, chesscom::Replay* response)
{
    if(!UserManager::Get()->Authenticate(request->user_ident())){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    // std::string test = "[Event \"Custom SlugChess game\"]\n" 
    //                     "[Site \"REDACTED, REDACTED NOR\"]\n" 
    //                     "[Date \"2020.10.8\"]\n" 
    //                     "[Round \"1\"]\n" 
    //                     "[White \"debugman\"]\n" 
    //                     "[Black \"debug_sexyman\"]\n" 
    //                     "[Result \"0-1\"]\n" 
    //                     "[Time \"19:3:23\"]\n" 
    //                     "[Mode \"ICS\"]\n" 
    //                     "[FEN \"knrqnbbr/pppppppp/8/8/8/8/PPPPPPPP/RQRNNBKB w - - 0 1\"]\n" 
    //                     "[SetUp \"1\"]\n" 
    //                     "[Variant \"SlugChess.Torch\"]\n" 
    //                     "\n" 
    //                     "1. e3 Nb8c6 2. c4 e5 3. g3 f6 4. Qb1e4 h6 5. Ne1d3 Bg8h7 6. Qe4f3 b6 7. Qf3e4 Qd8e7 8. Nd3c5 Bh7xe4 9. Bh1xe4 Qe7xc5 10. Be4xc6 d7xc6 11. e4 Qc5xf2 12. Kg1xf2 c5 13. b4 c5xb4 14. a3 b4xa3 15. Ra1xa3 c5 16. d3 Bf8d6 17. d4 e5xd4 18. Ra3d3 f5 19. e4xf5 g6 20. f5xg6 Rh8g8 21. Rd3xd4 Rg8xg6 22. Rd4xd6 Ne8xd6 23. Nd1e3 Rg6xg3 24. Kf2xg3 h5 25. Ne3f5 Nd6xf5 26. Bf1d3 Rc8f8 27. Bd3xf5 Rf8e8 28. Rc1b1 b5 29. c4xb5 a6 30. Bf5c8 Re8xc8 31. b5xa6 Rc8g8 32. Kg3h4 Rg8g4 33. Rb1b8 Rg4xh4  0-1";
    auto pgnMap = ReadSlugChessPgnString(request->pgn());
    auto sanMoves = San::SanMovesFromSan(pgnMap["San"]);

    auto* visionRule = SlugChess::GetVisionRule(pgnMap["Variant"].substr(pgnMap["Variant"].find('.')+1));
    if(visionRule == nullptr){
        Logger::log(Logger::LogLevel::Warning,fmt::format("Invalid visionrules '{}' requested", pgnMap["Variant"].substr(pgnMap["Variant"].find('.')+1)));
        Messenger::SendServerMessage(request->user_ident().usertoken(), 
            "PGN Invalid. Variant '"+pgnMap["Variant"]+"' unknown");
        response->set_valid(false);
        return grpc::Status::OK;
    }
    auto game = std::make_shared<SlugChess>(pgnMap["FEN"], *visionRule);
    auto moves = San::SanMovesFromSan(pgnMap["San"]);
    response->set_white(pgnMap["White"]);
    response->set_black(pgnMap["Black"]);
    SlugChessConverter::SetGameState(game, response->add_game_states(), PlayerTypes::Observer);
    for(auto& move : moves){
        if(!game->DoSanMove(move)){
            Logger::log(Logger::LogLevel::Warning,fmt::format("San move failed '{}'. Replay invalid", move));
            response->set_valid(false);
            return grpc::Status::OK;
        }else{
            //Logger::log(Logger::LogLevel::Debug,fmt::format("San move '{}'", move));
        }
        SlugChessConverter::SetGameState(game, response->add_game_states(), PlayerTypes::Observer);
    }
    switch (game->Result())
    {
    case SlugChess::EndResult::StillPlaying:
            response->set_match_event(chesscom::MATCH_EVENT_NON);
        break;
    case SlugChess::EndResult::Draw:
            response->set_match_event(chesscom::MATCH_EVENT_DRAW);
        break;
    case SlugChess::EndResult::WhiteWin:
            response->set_match_event(chesscom::MATCH_EVENT_WHITE_WIN);
        break;
    case SlugChess::EndResult::BlackWin:
            response->set_match_event(chesscom::MATCH_EVENT_BLACK_WIN);
        break;
    default:
        throw std::invalid_argument("WTF man. What the hell have you done??");
    }
    Logger::log(Logger::LogLevel::Info,"Replay parsed successfully");
    response->set_valid(true);
    return grpc::Status::OK;
}



grpc::Status ChessComService::GetPublicUserdata(grpc::ServerContext* context, const chesscom::UserDataRequest* request, chesscom::UserData* response)
{
    if(!UserManager::Get()->Authenticate(request->user_ident())){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    std::string usertoken = request->user_ident().usertoken();
    if(UserManager::Get()->UsertokenLoggedIn(usertoken)){
        auto userdata = UserManager::Get()->GetPublicUserDataFromUsername(request->username());
        if(userdata.username() == request->username()){
            *response = userdata;
            
            Logger::log(Logger::LogLevel::Info,fmt::format("Sending userdata about: {}", userdata.username()));
        }
    }
    return grpc::Status::OK;
}

grpc::Status ChessComService::QueryGamesDatabase(grpc::ServerContext* context, const chesscom::GamesDatabaseQuery* request, chesscom::GamesDatabaseResult* response)
{
    if(!UserManager::Get()->Authenticate(request->user_ident())){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    if(request->has_game_search()){
        auto query_result = DatabaseManager::Get()->QueryGamesDatabase(request->game_search());
        if(query_result.result_case() == chesscom::GamesDatabaseResult::ResultCase::kSearch){
            chesscom::GamesDatabaseFetch fetch;
            fetch.mutable_search_handle()->CopyFrom(query_result.search().search_handle());
            fetch.set_page_number(1);
            auto fetch_result = DatabaseManager::Get()->GamesDatabaseFetch(fetch);
            response->Swap(&fetch_result);
        } else { 
            response->Swap(&query_result);
        }
    }else if(request->has_games_fetch()){
        auto fetch_result = DatabaseManager::Get()->GamesDatabaseFetch(request->games_fetch());
        response->Swap(&fetch_result);
    }

    return grpc::Status::OK;
}

grpc::Status ChessComService::RequestPgnGame(grpc::ServerContext* context, const chesscom::PgnGameRequest* request, chesscom::GameResult* response) 
{
    if(!UserManager::Get()->Authenticate(request->user_ident())){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    response->set_pgn(Filesystem::GetPgnFromGamesDatabase(request->game()));
    return grpc::Status::OK;
}

grpc::Status ChessComService::PingService(grpc::ServerContext* context, grpc::ServerReaderWriter<chesscom::PingRequest, chesscom::PingResponse>* stream)
{
    chesscom::PingResponse msg;
    stream->Read(&msg);
    if(!UserManager::Get()->Authenticate(msg.user_ident())){
        return grpc::Status(grpc::UNAUTHENTICATED, "UserIdentification failed to authenticate");    
    }
    std::shared_ptr<bool> alive = std::make_shared<bool>(true);
    std::mutex mutex;
    std::unique_lock<std::mutex> lk(mutex);
    if(UserManager::Get()->AddUserToPingService(msg.user_ident().usertoken(), stream, alive)){
        chesscom::PingResponse ping_response;
        while (stream->Read(&ping_response) && !context->IsCancelled())
        {
            auto package_time = timeNow();
            //Logger::log(Logger::Info, fmt::format("Stop time {}", package_time.time_since_epoch()));
            if(!UserManager::Get()->UsertokenLoggedIn(msg.user_ident().usertoken()))
            {
                alive = nullptr;
                Logger::log(Logger::LogLevel::Info, fmt::format("PingService stream ended because {} logged out", msg.user_ident().usertoken()));
                return grpc::Status::OK;
            }else if(!UserManager::Get()->IsUsertokenActivePingService(msg.user_ident().usertoken())){
                alive = nullptr;
                Logger::log(Logger::LogLevel::Info, fmt::format("PingService stream ended because {} no longer in PingService map", msg.user_ident().usertoken()));
                return grpc::Status::OK;
            }
            UserManager::Get()->PingReport(msg.user_ident().usertoken(), package_time, ping_response.ping());
            //cv.wait_for(lk, std::chrono::milliseconds(MAX_SLEEP_MS));
        }
        return grpc::Status::OK;
    }else{
        alive = nullptr;
        stream->WriteLast(chesscom::PingRequest{}, {});
        return grpc::Status::OK;
    }
}