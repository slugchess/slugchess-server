/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include <string>
#include <chrono>
#include <memory>
#include <random>
#include <sstream>
#include "../chesscom/chesscom.grpc.pb.h"

#define TIMEOUT_FOR_DRAW 5
#define EXTRATIME_FOR_DRAW 1
#define MOVE_TIME_DELAY_ALLOWED_MILLISEC 1000

using time_point = std::chrono::time_point<std::chrono::system_clock,std::chrono::microseconds>;
using duration = std::chrono::duration<int64_t, std::micro>;

enum PlayerTypes{
    White = 0,
    Black = 1,
    Observer = 2
};
struct MoveResultStream {
    bool alive;
    grpc::internal::WriterInterface<chesscom::MoveResult>* streamPtr;
};
struct ChessClock {
    int blackSecLeft;
    int whiteSecLeft;
    int secsPerMove;
    bool is_ticking;
};
struct ChessMove {
    std::string from;
    std::string to;
};
struct Player
{
    PlayerTypes type;
    std::string usertoken;
    std::shared_ptr<MoveResultStream> resultStream;
    bool askingForDraw = false;
    time_point askingForDrawTimstamp;
    int64_t SecSinceAskedForDraw() { 
        return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - askingForDrawTimstamp).count(); }
};
struct PgnInfo
{
    std::string filename;
    std::string white_username;
    std::string black_username;
    time_point timestamp;
};

inline std::vector<std::string> tokenize(const std::string& text,const std::string& delimiters)
{
    std::vector<std::string> tokens;
    size_t current;
    size_t next = -1;
    do
    {
        current = next + 1;
        next = text.find_first_of( delimiters, current );
        tokens.push_back(text.substr( current, next - current ));
    }
    while (next != std::string::npos);
    return tokens;
}

inline std::string generate_random_alphanum(int max_length){
    std::string possible_characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    std::random_device rd;
    std::mt19937 engine(rd());
    std::uniform_int_distribution<> dist(0, possible_characters.size()-1);
    std::string ret = "";
    for(int i = 0; i < max_length; i++){
        int random_index = dist(engine); //get index between 0 and possible_characters.size()-1
        ret += possible_characters[random_index];
    }
    return ret;
}

inline uint64_t generate_random_uint64()
{
    std::random_device rd;
    std::mt19937_64 engine(rd());
    std::uniform_int_distribution<uint64_t> dist;
    return dist(engine);
}

/**
 *  Coverts to key value and san moves is behind "San" key
 *  ttime is official end of match time 
 */
inline std::map<std::string, std::string> ReadSlugChessPgnString(const std::string& pgn)
{
    std::map<std::string, std::string> map;
    std::istringstream istream(pgn);
    while (!istream.eof())
    {
        std::string line;
        std::getline(istream, line);
        if(istream.fail())return map;
        if(line[0] == '['){
            auto nameEndPos = line.find(' ', 1);
            auto startValue =  1 + line.find('"', nameEndPos);
            auto endValue =   line.find('"', startValue);
            map[line.substr(1, nameEndPos-1)] = line.substr(startValue, endValue-startValue);
        }
        else if(line[0] == '1')
        {
            std::string more_line;
            while (!istream.eof())
            {
                std::getline(istream, more_line);
                line = line + more_line;
            }
            map["San"] = line;            
        }
    }
    return map;
}

inline time_point timevalToTimePoint(timeval timeval){
  time_point converted{std::chrono::microseconds{timeval.tv_sec*1000*1000+timeval.tv_usec }};
  return converted;
}
inline timeval timePointToTimeval(time_point time_point){
    int64_t count = time_point.time_since_epoch().count();
    int64_t sec = count / 1000 / 1000;
    int64_t micro_res = count - (sec * 1000 * 1000);
    return timeval{sec, micro_res};
}
inline time_point timeNow(){
  return std::chrono::time_point_cast<std::chrono::microseconds>(std::chrono::system_clock::now());
}


