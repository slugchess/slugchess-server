/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include <condition_variable>
#include <map>
#include <chrono>
#include <memory>
#include <utility>
#include "messenger.h"
#include "common.h"
#include "slugchesscoverter.h"
#include "logger.h"
#include "slugchess.h"

class Match {
public:

    Match(const std::string& token, const std::string& whitePlayerToken, const std::string& blackPlayerToken, const std::vector<std::pair<std::string,PlayerTypes>>& observers, const std::string& fenString, const std::string& ruleType, VisionRules& visionRules);


    bool DoMove(const std::string& usertoken, std::shared_ptr<chesscom::Move> move, time_point move_received); 
    void PlayerDisconnected(const std::string& usertoken, chesscom::MatchEvent matchEventType);
    void PlayerListenerJoin(const std::string& usertoken, std::shared_ptr<MoveResultStream> resultStream);
    void PlayerListenerDisconnected(const std::string& usertoken);
    void PlayerAskingForDraw(const std::string& usertoken);
    void PlayerAcceptingDraw(const std::string& usertoken);
    std::string GetPgnString(time_t& ttime);
    static std::map<std::string, std::string> ReadSlugChessPgnString(const std::string& pgn);

    void SendMessageAllPlayers(const std::string& message);
    bool Ongoing(){ return !_matchFinished; }
    bool IsWhitesMove() { return moves.size()%2 == 0; }
    void CheckCurrentMoveOutOfTime();

    std::string& getWhitePlayer(){return _whitePlayer;}
    std::string& getBlackPlayer(){return _blackPlayer;}
    std::string& getMatchToken(){return _matchToken;}
    PlayerTypes getPlayerType(const std::string& usertoken){ return _players.at(usertoken).type; }

    std::shared_ptr<ChessClock> clock;
    std::shared_ptr<SlugChess> game;
    std::condition_variable cv;
    std::condition_variable matchDoneCV;

    std::vector<chesscom::MatchEvent> matchEvents;
    std::vector<std::shared_ptr<chesscom::Move>> moves;
 

    private:
    bool _matchFinished = false;
    std::string _whitePlayer;
    std::string _blackPlayer;
    std::string _matchToken;
    std::map<std::string, Player> _players;
    std::mutex _mutex;
    std::string _pgn = "";
    std::string _variant;
    time_point _last_move_sendt;

    void nl_SendMessageAllPlayers(const std::string& message);
    void nl_MatchCompleted(chesscom::MatchEvent result);
    void nl_TerminateMatch();
    void nl_SendEventCompleteAndTerminate(chesscom::MatchEvent match_event, bool move_happened){
        matchEvents.push_back(match_event);
        cv.notify_all();
        nl_MatchCompleted(match_event);
        nl_MatchEventAll(match_event, move_happened);
        nl_TerminateMatch();
    }
    void nl_MatchEventAskingForDraw(chesscom::MatchEvent matchEvent, std::string& usertoken);
    void nl_MatchEventAll(chesscom::MatchEvent matchEvent, bool moveHappened);
    duration nl_CalcMoveTimeSpent(const std::string& usertoken, std::pair<duration,duration> ping_offset, time_point move_received, int32_t self_reported_sec_spent, time_point self_reported_time_sendt);
};
