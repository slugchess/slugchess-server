/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include "mutex"
#include "condition_variable"
#include "tuple"
#include "future"
#include "queue"

#pragma GCC diagnostic push 
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#include "slugchess.h"
#include "../chesscom/chesscom.pb.h"
#pragma GCC diagnostic pop

#include "usermanager.h"

class GameBrowser{
    public:
    struct WaitingPlayer{
        int32_t gameId;
        std::condition_variable* cv;
        chesscom::LobbyState* lobbyState;
        bool* update;
    };
private:
    static GameBrowser* _instance;
    GameBrowser() {}
    std::atomic<int> _idCounter = 1000;
    std::mutex _availableGamesMutex;
    std::mutex _waitingPlayersMutex;  
    google::protobuf::Map<google::protobuf::int32, chesscom::HostedGame> _availableGames;
    std::map<std::string, WaitingPlayer> _waitingPlayers;

    std::queue<std::future<bool>> _eventProcessQueue;

    void AddPlayerChangedToEventQueue(int32_t id);
    void AddCancelHostGameToEventQueue(int id);

    std::pair<chesscom::LobbyCommandResultType, int32_t> IsHostAndWhatHostedGame(const std::string& usertoken);
    void StartGame(int32_t id, const std::string& requester_usertoken);
    void KickPlayer(int32_t id, const std::string& kick_usertoken);

    // Removes player from waiting (not lobby) and gives removed player LobbyEvent
    void nl_RemoveWaitingPlayer(const std::string& usertoken, chesscom::LobbyState_LobbyClosedReason removed_reason);
    void nl_RemoveWaitingPlayer(const std::string& usertoken, const chesscom::LobbyState& lobby_state);
    
    int GetNewId(){ return _idCounter++;}
    bool UsertokenInObservers(const std::string& usertoken, const chesscom::HostedGame& hostedGame)
    {
        auto it = std::find_if(hostedGame.observers().begin(), hostedGame.observers().end(), [&usertoken](const chesscom::UserData& ud){
            return ud.usertoken() == usertoken;
        });
        return it != hostedGame.observers().end();
    }

public:
    static GameBrowser* Get()
    {
        if(!_instance) _instance = new GameBrowser();
        return _instance;
    }
    void DoWork();
    int HostGame(const chesscom::HostedGame& hostGame, chesscom::LobbyState* matchResult, std::condition_variable* hostCV, bool* finished);
    void WriteAvailableGames(chesscom::HostedGamesMap& gamesList);
    bool JoinGame(int32_t id, const std::string& usertoken, bool observer, chesscom::LobbyState* lobbyState, std::condition_variable* hostCV, bool* update);
    void LeaveGame(int id, const std::string& usertoken);
    void UserLoggedOut(const std::string& token, std::shared_ptr<chesscom::UserData> userData);
    void UpdateLobby(const std::string& usertoken);

    chesscom::LobbyCommandResultType LobbyCommandStartGame(const std::string& usertoken);
    chesscom::LobbyCommandResultType LobbyCommandKickPlayer(const std::string& requester_usertoken, const std::string& kick_usertoken);



};
