/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#include "match.h"
#include "usermanager.h"
#include "filesystem.h"
#include <algorithm>


Match::Match(const std::string& token, const std::string& whitePlayerToken, const std::string& blackPlayerToken, const std::vector<std::pair<std::string,PlayerTypes>>& observers,
    const std::string& fenString, const std::string& variant, VisionRules& visionRules)
{
    _matchToken = token;
    _whitePlayer = whitePlayerToken;
    _blackPlayer = blackPlayerToken;
    _players[whitePlayerToken].usertoken = whitePlayerToken;
    _players[whitePlayerToken].type = PlayerTypes::White;
    _players[whitePlayerToken].askingForDrawTimstamp = timeNow();
    _players[blackPlayerToken].usertoken = blackPlayerToken;
    _players[blackPlayerToken].type = PlayerTypes::Black; 
    _players[blackPlayerToken].askingForDrawTimstamp = timeNow();
    for (auto& obs : observers) {
        _players[obs.first].usertoken = obs.first;
        _players[obs.first].type = obs.second;
    }
    _variant = variant;
    clock = std::make_shared<ChessClock>();
    game = std::make_shared<SlugChess>(fenString, visionRules);
    Logger::log(Logger::LogLevel::Debug, fmt::format("Creating match: {} white: {} black: {}", _matchToken, whitePlayerToken, blackPlayerToken));
}

bool Match::DoMove(const std::string& usertoken, std::shared_ptr<chesscom::Move> move, time_point move_received) 
{
    if(_matchFinished){ 
        Logger::log(Logger::LogLevel::Error,fmt::format("{} tried a move when match is finished", usertoken));
        return false;
    }
    if((IsWhitesMove() && _whitePlayer == usertoken)  
        || ((!IsWhitesMove()) && _blackPlayer == usertoken))
    {
        if(!UserManager::Get()->IsUsertokenActivePingService(usertoken)){
            Logger::log(Logger::LogLevel::Info,fmt::format("{} tried a move when ping service not active for usertoken", usertoken));
            chesscom::MoveResult mrPkt;
            mrPkt.set_move_happned(false);
            mrPkt.set_match_event(chesscom::MATCH_EVENT_PING_SERVICE_NOT_ACTIVE);
            _players.at(usertoken).resultStream->streamPtr->Write(mrPkt);
            return false;
        }
        std::unique_lock<std::mutex> scopeLock (_mutex);
        if(!game->LegalMove(move->from(), move->to())){ 
            Logger::log(Logger::LogLevel::Info, fmt::format("{} ERROR: tried a move that is not possible", usertoken));
            return false;
        }
        game->DoMove(move->from(), move->to());
        chesscom::MatchEvent expectedmatch_event = chesscom::MATCH_EVENT_NON;
        if(game->Result() != SlugChess::EndResult::StillPlaying){
            switch (game->Result())
            {
            case SlugChess::EndResult::Draw:
                expectedmatch_event = chesscom::MATCH_EVENT_DRAW;
                break;
            case SlugChess::EndResult::WhiteWin:
                expectedmatch_event = chesscom::MATCH_EVENT_WHITE_WIN;
                break;
            case SlugChess::EndResult::BlackWin:
                expectedmatch_event = chesscom::MATCH_EVENT_BLACK_WIN;
                break;
            default:
                break;
            }
        }
        if(moves.size() != 0){ //Avoid changing time first move
            auto time_spent = nl_CalcMoveTimeSpent(usertoken, UserManager::Get()->GetUserPingAndOffset(usertoken), 
                    move_received,
                    move->sec_spent(), 
                    timevalToTimePoint(google::protobuf::util::TimeUtil::TimestampToTimeval(move->timestamp()))); 
            
            int* secToChange = IsWhitesMove()? &clock->whiteSecLeft: &clock->blackSecLeft;
            *secToChange -= std::chrono::duration_cast<std::chrono::seconds>(time_spent).count();
            if(*secToChange <= 0){
                expectedmatch_event = IsWhitesMove()?chesscom::MATCH_EVENT_BLACK_WIN:chesscom::MATCH_EVENT_WHITE_WIN;
                nl_SendMessageAllPlayers(fmt::format("{} ran out of time", IsWhitesMove()?"white":"black"));
            }
            else {
                *secToChange += clock->secsPerMove;
            }
            
        }
        else
        {
            clock->is_ticking = true;
        }
        moves.push_back(move);
        if(expectedmatch_event != chesscom::MATCH_EVENT_NON){
            nl_SendEventCompleteAndTerminate(expectedmatch_event, true);
        } else {
            matchEvents.push_back(expectedmatch_event);
            cv.notify_all();
            nl_MatchEventAll(expectedmatch_event, true);
        }
        
        return true;
    }
    else
    {
        Logger::log(Logger::LogLevel::Info, fmt::format("{} ERROR: not this players turn to move", usertoken));
        return false;
    }
}

void Match::PlayerDisconnected(const std::string& usertoken, chesscom::MatchEvent matchEventType)
{
    SendMessageAllPlayers(UserManager::Get()->GetUserName(usertoken) + " left the game");

    std::unique_lock<std::mutex> scopeLock (_mutex);

    if(matchEventType == chesscom::MATCH_EVENT_WHITE_WIN || matchEventType == chesscom::MATCH_EVENT_BLACK_WIN ||
     matchEventType == chesscom::MATCH_EVENT_DRAW || matchEventType == chesscom::MATCH_EVENT_EXPECTED_CLOSING
     || matchEventType == chesscom::MATCH_EVENT_UNEXPECTED_CLOSING )
     {
         nl_SendEventCompleteAndTerminate(matchEventType, false);
     } else {
        matchEvents.push_back(matchEventType);
        cv.notify_all();
     }
}

void Match::PlayerListenerJoin(const std::string& usertoken, std::shared_ptr<MoveResultStream> resultStream )
{
    if(usertoken == _whitePlayer || usertoken == _blackPlayer)
    {
        //Allready have token and type
        std::unique_lock<std::mutex> scopeLock (_mutex);
        _players.at(usertoken).resultStream = resultStream;
    }
    else
    {
        //Is observer
        std::unique_lock<std::mutex> scopeLock (_mutex);
        _players.at(usertoken).usertoken = usertoken;
        _players.at(usertoken).type = PlayerTypes::Observer;
        _players.at(usertoken).resultStream = resultStream;
    }
}

void Match::PlayerListenerDisconnected(const std::string& usertoken)
{
    std::unique_lock<std::mutex> scopeLock (_mutex);
    if(_players.count(usertoken) > 0)
    {
        _players.at(usertoken).resultStream = nullptr;
    }
}

void Match::PlayerAskingForDraw(const std::string& usertoken)
{
    std::unique_lock<std::mutex> scopeLock (_mutex);
    std::string playerUsertoken, opponentUsertoken;
    if(_whitePlayer == usertoken){
        if(!IsWhitesMove())return;
        playerUsertoken = usertoken;
        opponentUsertoken = _blackPlayer;
    }else if(_blackPlayer == usertoken){
        if(IsWhitesMove())return;
        playerUsertoken = usertoken;
        opponentUsertoken = _whitePlayer;
    }else{
        Logger::log(Logger::LogLevel::Error, fmt::format("{} asked for draw but is not a player", usertoken));
        return;
    }
    if(_players.at(opponentUsertoken).SecSinceAskedForDraw() < TIMEOUT_FOR_DRAW)
    {
        //Opponent allready asked
        nl_SendEventCompleteAndTerminate(chesscom::MATCH_EVENT_DRAW, false);
        Logger::log(Logger::LogLevel::Debug, fmt::format("{} op allready asking for draw. SecSinceAsked {}", usertoken, _players[opponentUsertoken].SecSinceAskedForDraw()));
    }
    else if(_players.at(playerUsertoken).SecSinceAskedForDraw() > TIMEOUT_FOR_DRAW)
    {
        _players.at(playerUsertoken).askingForDrawTimstamp = timeNow();
        matchEvents.push_back(chesscom::MATCH_EVENT_ASKING_FOR_DRAW);
        cv.notify_all();
        //TORM newSystem
        nl_MatchEventAskingForDraw(chesscom::MATCH_EVENT_ASKING_FOR_DRAW, opponentUsertoken);
        Logger::log(Logger::LogLevel::Debug, fmt::format("{} asking for draw", usertoken));
        _players.at(playerUsertoken).SecSinceAskedForDraw();
    }else{
        Logger::log(Logger::LogLevel::Debug, fmt::format("{} allready asked for draw", usertoken));

    }
}

void Match::PlayerAcceptingDraw(const std::string& usertoken)
{
    std::unique_lock<std::mutex> scopeLock (_mutex);
    std::string playerUsertoken, opponentUsertoken;
    if(_whitePlayer == usertoken){
        playerUsertoken = usertoken;
        opponentUsertoken = _blackPlayer;
    }else if(_blackPlayer == usertoken){
        playerUsertoken = usertoken;
        opponentUsertoken = _whitePlayer;
    }else{
        Logger::log(Logger::LogLevel::Error, fmt::format("{} accepting draw but is not a player", usertoken));
        return;
    }
    if(_players.at(opponentUsertoken).SecSinceAskedForDraw() < TIMEOUT_FOR_DRAW + EXTRATIME_FOR_DRAW)
    {
        nl_SendEventCompleteAndTerminate(chesscom::MATCH_EVENT_DRAW, false);
        Logger::log(Logger::LogLevel::Debug, fmt::format("{} accepted draw for draw", usertoken));
    }else{
        Logger::log(Logger::LogLevel::Debug, fmt::format("{}acept draw failed. To many Sec since opp asked draw: {}", usertoken, _players.at(opponentUsertoken).SecSinceAskedForDraw()));
    }
}
/**
 *  ttime is official end of match time 
 */
std::string Match::GetPgnString(time_t& ttime)
{
    std::stringstream ss;
    ss << "[Event \"Custom SlugChess game\"]" << std::endl; // Games can not be custom when ladder is up
    ss << "[Site \"REDACTED, REDACTED NOR\"]" << std::endl;
    tm *local_time = gmtime(&ttime);
    ss << "[Date \""<< 1900 + local_time->tm_year << "." << std::setw(2) << std::setfill('0')
        << 1 + local_time->tm_mon << "." << local_time->tm_mday << "\"]" << std::setfill(' ') << std::endl;
    ss << "[Round \"1\"]" << std::endl;
    ss << "[White \"" << UserManager::Get()->GetUserName(_whitePlayer) << "\"]" << std::endl;
    ss << "[Black \"" << UserManager::Get()->GetUserName(_blackPlayer) << "\"]" << std::endl;
    ss << "[Result \"" << game->ResultString() << "\"]" << std::endl;
    ss << std::setw(2) << std::setfill('0');
    ss << "[Time \"" << local_time->tm_hour << ":" << local_time->tm_min << ":" << local_time->tm_sec << "\"]" << std::endl;
    ss << "[Mode \"ICS\"]" << std::endl;
    ss << "[FEN \"" << game->GetFenString() << "\"]" << std::endl;
    ss << "[SetUp \"1\"]" << std::endl;
    if(_variant != "custom")
    {
        ss << "[Variant \"SlugChess." << _variant << "\"]" << std::endl;
    } else {
        ss << "[Variant \"SlugChess." << "custom." << "{}" << "\"]" << std::endl;
    }
    
    ss << std::endl;
    game->PrintSanMoves(ss);
    return ss.str();
}

void Match::SendMessageAllPlayers(const std::string& message)
{
    std::unique_lock<std::mutex> scopeLock (_mutex);
    nl_SendMessageAllPlayers(message);
}

void Match::CheckCurrentMoveOutOfTime() { 
    if(Ongoing() && clock->is_ticking ) {
        std::unique_lock<std::mutex> scopeLock (_mutex);
        std::chrono::seconds timeLeft;
        if(IsWhitesMove()){
            timeLeft = std::chrono::seconds(clock->whiteSecLeft);
        } else {
            timeLeft = std::chrono::seconds(clock->blackSecLeft);
        }
        if(timeNow() > _last_move_sendt + timeLeft + std::chrono::seconds(2)){
            Logger::log(Logger::LogLevel::Debug, fmt::format("Match {}, player to late with move. Ending match", _matchToken));
            auto matchEvent = IsWhitesMove()?chesscom::MATCH_EVENT_BLACK_WIN:chesscom::MATCH_EVENT_WHITE_WIN;
            nl_SendMessageAllPlayers(fmt::format("{} ran out of time", IsWhitesMove()?"white":"black"));
            nl_SendEventCompleteAndTerminate(matchEvent, false);
        };
    }
}

void Match::nl_SendMessageAllPlayers(const std::string& message)
{
    for(auto& [usertoken, player]  : _players)
    {
        if(player.resultStream != nullptr) //Indicates player is connected to match and wants to listen
        {
            Messenger::SendServerMessage(usertoken, message);
        }
    }
}

void Match::nl_MatchCompleted(chesscom::MatchEvent result)
{
    time_t endOfMatch = time(nullptr);
    _pgn = GetPgnString(endOfMatch);
    Filesystem::WriteMatchPgn(
        UserManager::Get()->GetUserName(_whitePlayer), 
        UserManager::Get()->GetUserName(_blackPlayer),
        _pgn,
        endOfMatch);
    UserManager::Get()->UpdateElo(_whitePlayer, _blackPlayer, result);
    _matchFinished = true;
}

void Match::nl_TerminateMatch()
{
    Logger::log(Logger::LogLevel::Debug,_matchToken + " terminating match");
    matchDoneCV.notify_all();
}
void Match::nl_MatchEventAskingForDraw(chesscom::MatchEvent matchEvent, std::string& usertoken)
{
    chesscom::MoveResult mrPkt;
    mrPkt.set_move_happned(false);
    mrPkt.set_match_event(matchEvent);

    if(_players.count(usertoken) > 0)
    {
        if(_players.at(usertoken).resultStream != nullptr && _players.at(usertoken).resultStream->alive ) 
        {
            _players.at(usertoken).resultStream->streamPtr->Write(mrPkt);
        }
    }
}

void Match::nl_MatchEventAll(chesscom::MatchEvent matchEvent, bool moveHappened)
{
    //Logger::log(Logger::LogLevel::Info,"MatchEvent " + std::to_string(matchEvent));
    chesscom::MoveResult mrPkt;

    chesscom::GameState whiteGS;
    chesscom::GameState blackGS;
    chesscom::GameState observerGS;
    if(moveHappened){
        mrPkt.set_move_happned(true);
        SlugChessConverter::SetGameState(game, &whiteGS, PlayerTypes::White);
        SlugChessConverter::SetGameState(game, &blackGS, PlayerTypes::Black);
        SlugChessConverter::SetGameState(game, &observerGS, PlayerTypes::Observer);
    }else{
        mrPkt.set_move_happned(false);
    }
    if(_matchFinished){
        mrPkt.mutable_game_result()->set_pgn(_pgn);

    }
    
    mrPkt.set_match_event(matchEvent);
    mrPkt.mutable_chess_clock()->set_white_seconds_left(clock->whiteSecLeft);
    mrPkt.mutable_chess_clock()->set_black_seconds_left(clock->blackSecLeft);
    mrPkt.mutable_chess_clock()->set_timer_ticking(clock->is_ticking);

    for(auto& [_, player]  : _players)
    {
        //(void)usertoken;//To suppress unused variable warning
        if(player.resultStream != nullptr && player.resultStream->alive) 
        {
            if(moveHappened) {
                switch(player.type){
                    case PlayerTypes::White:
                        mrPkt.set_allocated_game_state(&(whiteGS));        
                    break;
                    case PlayerTypes::Black:
                        mrPkt.set_allocated_game_state(&(blackGS));        
                    break;
                    case PlayerTypes::Observer:
                        mrPkt.set_allocated_game_state(&(observerGS));        
                    break;
                }
            }
            if(_matchFinished){
                player.resultStream->streamPtr->WriteLast(mrPkt, {});
            }else{
                player.resultStream->streamPtr->Write(mrPkt);
            }
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
            if(moveHappened) mrPkt.release_game_state();
#pragma GCC diagnostic pop
        }
        if(moveHappened){
            _last_move_sendt = timeNow();
        }
    }

}

duration Match::nl_CalcMoveTimeSpent(const std::string& usertoken, std::pair<duration,duration> ping_offset, time_point move_received, int32_t self_reported_sec_spent, time_point self_reported_time_sendt)
{
    auto& [ping, offset] = ping_offset;
    auto wiggleroom = std::chrono::milliseconds(MOVE_TIME_WIGGLEROOM_MS);
    auto user_sec_spent = std::chrono::seconds(self_reported_sec_spent);
    auto time_since_sent = move_received - _last_move_sendt;
    auto user_reported_time = user_sec_spent + ping*2 + offset;
    auto user_time_server_reveive_estimate = self_reported_time_sendt + offset + ping;

    bool trust_self_reported_time_sendt = user_time_server_reveive_estimate > move_received - wiggleroom && 
                                           user_time_server_reveive_estimate < move_received + wiggleroom;
    if(trust_self_reported_time_sendt && user_sec_spent == std::chrono::duration_cast<std::chrono::seconds>(self_reported_time_sendt + offset - (_last_move_sendt + ping))){
        return std::chrono::seconds(self_reported_sec_spent);
    }
    Logger::log(Logger::Debug, fmt::format("Not trusting {} trust_self_reported_time_sendt", usertoken));
    // minus 1 sec as that is the resolution of the clock
    bool trust_self_reported = user_reported_time > time_since_sent + std::chrono::seconds(-1) - wiggleroom &&
                                user_reported_time < time_since_sent + wiggleroom;
    if(trust_self_reported){
        return std::chrono::seconds(self_reported_sec_spent);
    }else {
        Logger::log(Logger::Debug, fmt::format("Move time '{}' not trusted using server time_since_sent. Self reported sec spent: {}, User reported time: {}, Ping {}, Offset {}, Server time since sent: {}",
            usertoken, self_reported_sec_spent, 
            std::chrono::duration_cast<std::chrono::milliseconds>(user_reported_time), 
            std::chrono::duration_cast<std::chrono::milliseconds>(ping), 
            std::chrono::duration_cast<std::chrono::milliseconds>(offset), 
            std::chrono::duration_cast<std::chrono::milliseconds>(time_since_sent)));
        return std::chrono::duration_cast<std::chrono::seconds>(time_since_sent); 
        //return time_since_sent - std::chrono::milliseconds(MOVE_TIME_WIGGLEROOM_MS);
    }
    
}