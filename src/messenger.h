/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include <map>
#include <memory>
#include "../chesscom/chesscom.grpc.pb.h"

class Messenger {
    public:
    static void SendServerMessage(const std::string& revicerToken, const std::string& message);
    static void SendMessage(std::string& revicerToken, std::string& senderUername, std::string& message);
    static void SendMessage(const std::string& revicerToken, const std::string& senderUername, const std::string& message);
    private:
    //static std::mutex _logMutex; //static requires init on messanger.cpp
    
};
