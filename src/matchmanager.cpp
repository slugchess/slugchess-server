/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#include "matchmanager.h"
#include "slugchesscoverter.h"
#include <random>
#include <exception>
#include <string>

MatchManager* MatchManager::_instance = 0;

std::string MatchManager::CreateMatch(std::string& player1Token, std::string& player2Token)
{
    // std::string matchToken = "match"+ std::to_string(_tokenCounter++);
    // std::string white;
    // std::string black; 
    // std::tie(white, black) = RandomSort(player1Token, player2Token);
    // std::shared_ptr<::Match> match = std::make_shared<::Match>(matchToken, white, black, std::vector<std::pair<std::string,PlayerTypes>>{},	 
    //     "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w AHah - 0 1", "custom", defaultVisionRules);

    // match->clock->blackSecLeft = defaultTimeRules.player_time().minutes() * 60 + defaultTimeRules.player_time().seconds();
    // match->clock->whiteSecLeft = match->clock->blackSecLeft;
    // match->clock->secsPerMove = defaultTimeRules.seconds_per_move();
    // match->clock->is_ticking = false;
    // _matches[matchToken] = match;
    // // Logger::log(Logger::LogLevel::Info, fmt::format("  checing match, black sec left {} white sec left {}", match->clock->blackSecLeft, match->clock->whiteSecLeft));
    // auto matPtr = _matches[matchToken];
    //return matchToken;
    return "";
}
std::string MatchManager::CreateMatch(chesscom::HostedGame& hostedGame)
{
    std::string matchToken = "match"+ std::to_string(_tokenCounter++);
    std::string white;
    std::string black;
    if(hostedGame.game_rules().side_type() == chesscom::SIDE_TYPE_HOST_IS_WHITE)
    {
        white = hostedGame.host().usertoken();black = hostedGame.joiner().usertoken();
    }
    else if(hostedGame.game_rules().side_type() == chesscom::SIDE_TYPE_HOST_IS_BLACK)
    {
        black = hostedGame.host().usertoken();white = hostedGame.joiner().usertoken();
    }
    else
    {
        std::tie(white, black) = RandomSort(hostedGame.host().usertoken(), hostedGame.joiner().usertoken());
    }
    std::string fenString;
    std::string variant;
    VisionRules visionRules;
    if(hostedGame.game_rules().variant_case() == chesscom::GameRules::VariantCase::kNamed)
    {
        variant = hostedGame.game_rules().named();
        if(variant == "Classic"){
            fenString = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w AHah - 0 1";
        }else{
            fenString = Sfen::GenSlugRandom();
        }
        visionRules = *SlugChess::GetVisionRule(variant);
    }else{ 
        variant = "custom";
        //visionRules = FromChesscomVisionRules(hostedGame.game_rules().custom_rules());
    }
    std::vector<std::pair<std::string,PlayerTypes>> observers;
    for (auto& obs : hostedGame.observers()){
        observers.push_back({obs.usertoken(), PlayerTypes::Observer});
    }
    std::shared_ptr<::Match> match = std::make_shared<::Match>(matchToken, white, black, observers, fenString, variant, visionRules);
    match->clock->blackSecLeft = hostedGame.game_rules().time_rules().player_time().minutes() * 60 + hostedGame.game_rules().time_rules().player_time().seconds();
    match->clock->whiteSecLeft = match->clock->blackSecLeft;
    match->clock->secsPerMove = hostedGame.game_rules().time_rules().seconds_per_move();
    match->clock->is_ticking = false;
    {
        std::unique_lock<std::mutex> lk(_matchesMutex);
        _matches[matchToken] = match;
    }
    return matchToken;
}

std::shared_ptr<Match> MatchManager::GetMatch(const std::string& matchId)
{
    std::unique_lock<std::mutex> lk(_matchesMutex);
    if(_matches.count(matchId) > 0)
    {
        return _matches[matchId];
    }else{
        return std::shared_ptr<Match>(nullptr);
    }
}

void MatchManager::EraseMatch(const std::string& matchId)
{
    std::unique_lock<std::mutex> lk(_matchesMutex);
    _matches.erase(matchId);
}

void MatchManager::UserLoggedOut(const std::string& token, std::shared_ptr<chesscom::UserData> userData)
{
    std::unique_lock<std::mutex> lk(_matchesMutex);
    for(auto& [matchId, matchPtr] : _matches)
    {
        if(matchPtr->getWhitePlayer() == token)
        {
            Logger::log(Logger::LogLevel::Info, fmt::format("Ended match {} because {} got logged out", matchId, token));
            if( matchPtr->moves.size() > 0){
                matchPtr->PlayerDisconnected(token, chesscom::MATCH_EVENT_BLACK_WIN);
            }else {
                matchPtr->PlayerDisconnected(token, chesscom::MATCH_EVENT_DRAW);
            }
        }
        else if(matchPtr->getBlackPlayer() == token)
        {
            Logger::log(Logger::LogLevel::Info, fmt::format("Ended match {} because {} got logged out", matchId, token));
            if( matchPtr->moves.size() > 0){
                matchPtr->PlayerDisconnected(token, chesscom::MATCH_EVENT_WHITE_WIN);
            }else {
                matchPtr->PlayerDisconnected(token, chesscom::MATCH_EVENT_DRAW);
            }
        }
    }
    
}

VisionRules MatchManager::FromChesscomVisionRules(const chesscom::VisionRules& chesscomVision)
{
    VisionRules visionRules;
    visionRules.enabled = chesscomVision.enabled();
    visionRules.globalRules.ViewMoveFields = chesscomVision.view_move_fields();
    visionRules.globalRules.ViewRange = chesscomVision.view_range();
    visionRules.globalRules.ViewCaptureField = chesscomVision.view_capture_field();
    for (auto&& pieceRulesPar : chesscomVision.piece_overwriter()) {
        ChessPiece piece = (ChessPiece)pieceRulesPar.first;
        visionRules.overWriteRules[piece].ViewRange = pieceRulesPar.second.view_range();
        visionRules.overWriteRules[piece].ViewMoveFields = pieceRulesPar.second.view_move_fields();
        visionRules.overWriteRules[piece].ViewCaptureField = pieceRulesPar.second.view_capture_field();
    }

    return visionRules;
}

chesscom::VisionRules MatchManager::FromSlugChessVisionRules(const VisionRules& vr)
{
    chesscom::VisionRules ccVr;
    ccVr.set_enabled(vr.enabled);
    ccVr.set_view_move_fields(vr.globalRules.ViewMoveFields);
    ccVr.set_view_range(vr.globalRules.ViewRange);
    ccVr.set_view_capture_field(vr.globalRules.ViewCaptureField);
    for (auto&& [cp, rules] : vr.overWriteRules) {
        chesscom::Pieces piece = (chesscom::Pieces)cp;
        auto& ccOverRule = (*ccVr.mutable_piece_overwriter())[piece];
        ccOverRule.set_view_range(rules.ViewRange);
        ccOverRule.set_view_move_fields(rules.ViewMoveFields);
        ccOverRule.set_view_capture_field(rules.ViewCaptureField);
    }

    return ccVr;
}

std::pair<std::string,std::string> MatchManager::RandomSort(const std::string& first,const std::string& second)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist(0, 1);
    if(dist(mt) == 1)
    {
        return {first, second};
    }
    else
    {
        return {second, first};
    }
}

void MatchManager::MatchListenLoop(
        std::string listenerUsertoken, 
        std::shared_ptr<Match> matchPtr,
        grpc::ServerContext* contextPtr,
        grpc::internal::WriterInterface<chesscom::MoveResult>* writerPtr)
{
    chesscom::MoveResult moveResultPkt;
    chesscom::GameState state;
    //bool playerIsWhite = matchPtr->getWhitePlayer() == listenerUsertoken;
    PlayerTypes playerType = matchPtr->getPlayerType(listenerUsertoken);
    //bool loop = true;
    //uint lastEventNum = 0;
    std::mutex mutex;
    std::unique_lock<std::mutex> lk(mutex);
    //Sending init
    //Logger::log(Logger::LogLevel::Debug, fmt::format(" Sending init gamestate to  {} ", listenerUsertoken));
    moveResultPkt.set_move_happned(false);
    SlugChessConverter::SetGameState(matchPtr->game, &state, playerType);
    moveResultPkt.set_allocated_game_state(&state);
    moveResultPkt.set_match_event(chesscom::MATCH_EVENT_NON);
    moveResultPkt.mutable_chess_clock()->set_white_seconds_left(matchPtr->clock->whiteSecLeft);
    moveResultPkt.mutable_chess_clock()->set_black_seconds_left(matchPtr->clock->blackSecLeft);
    moveResultPkt.mutable_chess_clock()->set_timer_ticking(matchPtr->clock->is_ticking);
    writerPtr->Write(moveResultPkt);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
    moveResultPkt.release_game_state();
#pragma GCC diagnostic pop
    std::shared_ptr<MoveResultStream> resultStream = std::make_shared<MoveResultStream>();
    resultStream->streamPtr = writerPtr;
    resultStream->alive = true;
    matchPtr->PlayerListenerJoin(listenerUsertoken, resultStream);
    while(matchPtr->Ongoing() && !contextPtr->IsCancelled())
    {
        matchPtr->matchDoneCV.wait_for(lk, std::chrono::milliseconds(MAX_SLEEP_MS));
    }
    resultStream->alive = false;
    matchPtr->PlayerListenerDisconnected(listenerUsertoken);
}

void MatchManager::DoMoveInMatch(
        chesscom::MatchEvent event,
        std::string usertoken, 
        std::shared_ptr<Match> matchPtr, 
        std::shared_ptr<chesscom::Move> movePtr,
        time_point move_received)
{
    
    switch (event)
    {
    case chesscom::MATCH_EVENT_NON:
    {
        bool didMove = matchPtr->DoMove(usertoken, movePtr, move_received);
        if(didMove){
            // add log message here
        }
    }
        break;
    case chesscom::MATCH_EVENT_UNEXPECTED_CLOSING:
    {
        
        if(matchPtr->getWhitePlayer() == usertoken && matchPtr->moves.size() > 0)
        {
            matchPtr->PlayerDisconnected(usertoken, chesscom::MATCH_EVENT_BLACK_WIN);
        }
        else if(matchPtr->getBlackPlayer() == usertoken && matchPtr->moves.size() > 0)
        {
            matchPtr->PlayerDisconnected(usertoken, chesscom::MATCH_EVENT_WHITE_WIN);
        }else{
            matchPtr->PlayerDisconnected(usertoken, event);
        }
        
    }
        break;
    case chesscom::MATCH_EVENT_EXPECTED_CLOSING:
    {
        if(matchPtr->getWhitePlayer() == usertoken && matchPtr->moves.size() > 0)
        {
            matchPtr->PlayerDisconnected(usertoken, chesscom::MATCH_EVENT_BLACK_WIN);
        }
        else if(matchPtr->getBlackPlayer() == usertoken && matchPtr->moves.size() > 0)
        {
            matchPtr->PlayerDisconnected(usertoken, chesscom::MATCH_EVENT_WHITE_WIN);
        }else{
            matchPtr->PlayerDisconnected(usertoken, event);
        }
    }
        break;
    case chesscom::MATCH_EVENT_ASKING_FOR_DRAW:
    {
        matchPtr->PlayerAskingForDraw(usertoken);
    }
        break;
    case chesscom::MATCH_EVENT_ACCEPTING_DRAW:
    {
        matchPtr->PlayerAcceptingDraw(usertoken);
    }
        break;
    default:
        throw "fuck fwafdw";
        break;
    }
}

void MatchManager::DoWork(){
    auto matches = _matches;
    for (auto& [matchToken, match_ptr] : matches){
        match_ptr->CheckCurrentMoveOutOfTime();
    }
}
