/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include <mutex>
#include <string>
#include <unordered_map>
#include <condition_variable>
#include <chrono>
#include <fmt/chrono.h>

#include "../chesscom/chesscom.grpc.pb.h"
#include "userstore.h"
#include "filesystem.h"

/*struct MessagePack
{
    grpc::internal::WriterInterface<chesscom::ChatMessage>* writer = nullptr;
    std::condition_variable* messageListenerCV;
    std::mutex writerLock;
};*/

struct User
{
    std::shared_ptr<chesscom::UserData> data;
    time_point lastHeartbeat;
    grpc::internal::WriterInterface<chesscom::ChatMessage>* messageStream = nullptr;
    std::mutex messageStreamMutex;
    std::condition_variable changedCV;
    std::vector<unsigned char> encryptionKey;
    std::string session_secret;
    time_point lastDataUpdate;
    
    duration ping_value;
    duration estimated_clock_offset;
};

struct PingData
{
    grpc::ServerReaderWriter<chesscom::PingRequest, chesscom::PingResponse>* stream;
    std::weak_ptr<bool> alive;
    std::map<u_int64_t, time_point> pings_sent;
};

//PLEASE NOTE: ONE CAN NOT ASSUME A USER IS LOGGED IN FROM ONE PUBLIC CALL TO ANOTHER
//   SOME REWORK OF FUNCTIONS IS NESSESARY FOR THAT
class UserManager
{
    private:
    static UserManager* _instance;
    UserManager() {}
    ~UserManager();
    std::mutex _mutex;
    std::unordered_map<std::string, User> _logedInUsers;
    std::mutex _ping_mutex;
    std::unordered_map<std::string, std::shared_ptr<PingData>> _ping_service_users;

    public:
    static UserManager* Get()
    {
        if(!_instance) 
        _instance = new UserManager();
        return _instance;
    }
    static void DeleteInstance(){ if(_instance != nullptr) {delete _instance; _instance = nullptr;}}

    std::string LogInUser(const std::string& token, const chesscom::UserData& userData, const std::vector<unsigned char>& encryptionKey);
    bool UsertokenLoggedIn(const std::string& token);
    bool Authenticate(const chesscom::UserIdentification& ident);
    bool IsUsertokenActivePingService(const std::string& token);
    bool Heartbeat(const std::string& token);
    bool TestHeart(const std::string& token);
    bool Logout(const std::string& token);
    void PingReport(const std::string& usertoken, time_point server_receive_time, const chesscom::PingResponse::Ping& ping);
    std::shared_ptr<chesscom::UserData> GetUserData(const std::string& token);
    chesscom::UserData GetPublicUserDataFromUsername(const std::string& username);
    User* GetUser(const std::string& token);
    std::string GetUserName(const std::string& token);
    const std::vector<unsigned char>& GetEncryptionKey(const std::string& token);
    std::pair<duration,duration> GetUserPingAndOffset(const std::string& token);
    int ActiveUsers() { return _logedInUsers.size(); }
    void CheckHeartbeat();
    void CheckPing();
    void UpdateElo(const std::string& white_usertoken, const std::string& black_usertoken, chesscom::MatchEvent result);

    bool AddUserToPingService(const std::string& usertoken, grpc::ServerReaderWriter<chesscom::PingRequest,chesscom::PingResponse>* stream, std::weak_ptr<bool> alive);
    void AddMessageStream(const std::string& usertoken, grpc::internal::WriterInterface< chesscom::ChatMessage>* stream);
    void RemoveMessageStream(const std::string& usertoken);
    
    
};
