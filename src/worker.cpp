/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#include "worker.h"
#include "usermanager.h"
#include "gamebrowser.h"

Worker* Worker::_instance = 0;

void Worker::Start()
{
    if(!_instance){ 
        Logger::log(Logger::LogLevel::Info, fmt::format("WorkThread Starting"));
        _instance = new Worker();
        _instance->_worker = std::thread([](){_instance->WorkLoop();});
    }
    _instance->_stop_worker = false;
}

void Worker::Stop()
{
    if(_instance){ 
        _instance->_stop_worker = true;
    }
}

void Worker::Join()
{
    if(_instance && _instance->_worker.joinable()){ 
        _instance->_worker.join();
    }
}

void Worker::WorkLoop()
{
    std::chrono::time_point runStart = std::chrono::system_clock::now();
    while(!_instance->_stop_worker)
    {
        runStart = std::chrono::system_clock::now();
        if(_addWorkList.size() > 0){
            std::scoped_lock<std::mutex> lock(_changeLock);
            for (auto &&work : _addWorkList)
            {
                _workList.push_back(work);
            }
            _addWorkList.clear();
        }
        if(NeedIteration()){
            std::scoped_lock<std::mutex> lock(_workLock);
            //Call Work1 ..
            //UserManager::Get()->DoWork()
            //MatchManager::Get()->DoWork()
            GameBrowser::Get()->DoWork();
            for (auto &&work : _workList)
            {
                if(std::chrono::system_clock::now() > _lastTimeWorkedOn[work->id] + work->interval)
                {
                    _lastTimeWorkedOn[work->id] = std::chrono::system_clock::now();
                    work->func();
                }
            }
            
        }
        std::this_thread::sleep_until(runStart + std::chrono::milliseconds(WORK_INTERVAL_MILI));
    }
}

bool Worker::NeedIteration()
{
    return UserManager::Get()->ActiveUsers() > 0;
}

bool Worker::AddWork(std::shared_ptr<Work> work)
{
    std::scoped_lock<std::mutex> lock(_instance->_changeLock);
    if(std::find(_instance->_workList.begin(), _instance->_workList.end(), work) == _instance->_workList.end())
    {
        _instance->_addWorkList.push_back(work);
        return true;
    }else{
        //Allready contains work with id
        return false;
    }
}
// void Worker::RemoveWork(std::function<void()> func)
// {

// }


// void Worker::w_AddRemoveFunctions()
// {
//     std::scoped_lock<std::mutex> lock(_changeLock);
//     //_workFunctions.
// }
