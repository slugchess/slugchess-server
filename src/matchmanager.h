/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include "../chesscom/chesscom.grpc.pb.h"
#include "match.h"
#include "consts.h"

class MatchManager
{
    private:
    static MatchManager* _instance;
    MatchManager(){ }

    std::mutex _matchesMutex;
    std::map<std::string, std::shared_ptr<::Match>> _matches;
    std::atomic<int> _tokenCounter;

    std::pair<std::string,std::string> RandomSort(const std::string& first, const std::string& second);

    public:
    static MatchManager* Get()
    {
        if (!_instance)
        _instance = new MatchManager();
        return _instance;
    }
    static void MatchListenLoop(std::string listenerUsertoken, 
        std::shared_ptr<Match> matchPtr,
        grpc::ServerContext* contextPtr,
        grpc::internal::WriterInterface<chesscom::MoveResult>* writerPtr);
    static void DoMoveInMatch(
        chesscom::MatchEvent event,
        std::string usertoken, 
        std::shared_ptr<Match> matchPtr, 
        std::shared_ptr<chesscom::Move> movePtr,
        time_point move_received);

    std::string CreateMatch(std::string& player1Token, std::string& player2Token);
    std::string CreateMatch(chesscom::HostedGame& hostedGame);
    std::shared_ptr<Match> GetMatch(const std::string& matchId);
    void EraseMatch(const std::string& matchId);
    int OngoingMatches() { return _matches.size(); }
    VisionRules FromChesscomVisionRules(const chesscom::VisionRules& chesscomVision);
    chesscom::VisionRules FromSlugChessVisionRules(const VisionRules& vr);
    void UserLoggedOut(const std::string& token, std::shared_ptr<chesscom::UserData> userData);

    void DoWork();

    chesscom::TimeRules ServerTimeRules()
    {
        chesscom::TimeRules tr;
        tr.mutable_player_time()->set_minutes(5);
        tr.mutable_player_time()->set_seconds(0);
        tr.set_seconds_per_move(6);

        return tr;
    }

    VisionRules ServerVisionRules() 
    {
        VisionRules rules;
        rules.globalRules = Rules();
        rules.globalRules.ViewCaptureField = true;
        rules.globalRules.ViewMoveFields = false;
        rules.globalRules.ViewRange = 2;
        rules.enabled = true;
        rules.overWriteRules[ChessPiece::WhitePawn] = Rules(false, true, 1);
        rules.overWriteRules[ChessPiece::BlackPawn] = Rules(false, true, 1);
	
        return rules;
    }
};
