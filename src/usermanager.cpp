/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#include "usermanager.h"
#include "matchmanager.h"
#include "gamebrowser.h"
UserManager* UserManager::_instance = nullptr;

UserManager::~UserManager(){
    for (auto& [usertoken, user] : _logedInUsers)
    {
        UserStore::WriteUserEloToFile(user.data, user.encryptionKey);
        Logger::log(Logger::LogLevel::Debug,"Wrote user ELO on quit: " + user.data->username());
    }
    
}
std::string UserManager::LogInUser(const std::string& token, const chesscom::UserData& userData, const std::vector<unsigned char>& encryptionKey)
{
    std::scoped_lock<std::mutex> lock(_mutex);
    _logedInUsers[token].data = std::make_shared<chesscom::UserData>();
    _logedInUsers[token].data.get()->set_username(userData.username());
    _logedInUsers[token].data.get()->set_usertoken(userData.usertoken());
    _logedInUsers[token].data.get()->set_elo(userData.elo());
    _logedInUsers[token].lastHeartbeat = timeNow();
    _logedInUsers[token].encryptionKey = encryptionKey;
    _logedInUsers[token].session_secret = generate_random_alphanum(10);
    return _logedInUsers[token].session_secret;
}

bool UserManager::UsertokenLoggedIn(const std::string& token)
{
    return _logedInUsers.count(token) > 0;
}

bool UserManager::Authenticate(const chesscom::UserIdentification& ident)
{
    bool result = false;
    std::string secret = "-1";
    {
        std::scoped_lock<std::mutex> lock(_mutex);
        if(_logedInUsers.count(ident.usertoken()) > 0){
            secret = _logedInUsers.at(ident.usertoken()).session_secret;            
        } 
    }
    if(secret == ident.secret()){
        result = true;
    }else {
        Logger::log(Logger::LogLevel::Warning,fmt::format("Authentication secret '{}' for '{}' not correct. Should be '{}'", ident.secret(), ident.usertoken(), _logedInUsers.at(ident.usertoken()).session_secret));
    }
    return result; 
}

bool UserManager::IsUsertokenActivePingService(const std::string& token)
{
    return _ping_service_users.count(token) > 0;
}

bool UserManager::Heartbeat(const std::string& token)
{
    if(TestHeart(token)){
        _logedInUsers.at(token).lastHeartbeat = timeNow();
        return true;
    }
    else
    {
        return false;
    }
}

bool UserManager::TestHeart(const std::string& token)
{
    if(UsertokenLoggedIn(token)){
        std::scoped_lock<std::mutex> lock(_mutex);
        auto now = std::chrono::system_clock::now();
        Logger::log(Logger::LogLevel::Debug, fmt::format("{} duration since last heartbeat {}", token
            , std::chrono::duration_cast<std::chrono::milliseconds>(now - _logedInUsers.at(token).lastHeartbeat).count()));
        if(std::chrono::duration_cast<std::chrono::milliseconds>(now - _logedInUsers.at(token).lastHeartbeat) 
            > std::chrono::milliseconds(1000*60*2))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
    
    
}

bool UserManager::Logout(const std::string& token)
{
    std::shared_ptr<chesscom::UserData> userdata;
    std::vector<uint8_t> encryptionKey;
    {
        std::scoped_lock<std::mutex> lock(_mutex);
        if(_logedInUsers.count(token) > 0){
            userdata = _logedInUsers.at(token).data;
            encryptionKey = _logedInUsers.at(token).encryptionKey;
            _logedInUsers.at(token).changedCV.notify_all();
            _logedInUsers.erase(token);
            std::scoped_lock lock2(_ping_mutex);
            _ping_service_users.erase(token);
        }else{
            return false;
        }
        
    }
    UserStore::Get()->WriteUserEloToFile(userdata, encryptionKey);
    //Add calls to managers that want to be notified of this
    GameBrowser::Get()->UserLoggedOut(token, userdata);
    MatchManager::Get()->UserLoggedOut(token, userdata);
    return true;
}

void UserManager::PingReport(const std::string& usertoken, time_point server_receive_time, const chesscom::PingResponse::Ping& ping)
{
    duration time_offset, timediff;
    bool success = false;
    {
        std::unique_lock scopeLock (_ping_mutex);
        if(_ping_service_users.count(usertoken) > 0){
            auto& ping_data = _ping_service_users.at(usertoken);
            if(ping_data->pings_sent.count(ping.token()) > 0){
                time_point server_send_time = ping_data->pings_sent.at(ping.token());
                timediff = server_receive_time - server_send_time;
                auto sent_time = timevalToTimePoint(google::protobuf::util::TimeUtil::TimestampToTimeval(ping.timestamp()));
                time_offset = sent_time - server_send_time;
                success = true;
                ping_data->pings_sent.erase(ping.token());
            }
        }
    }
    if(success){
        std::scoped_lock<std::mutex> lock(_mutex);
        if(_logedInUsers.count(usertoken) > 0){
            (_logedInUsers.at(usertoken).ping_value += timediff) /= 2;   
            (_logedInUsers.at(usertoken).estimated_clock_offset += time_offset) /= 2;
        } 
    }
    
}

std::shared_ptr<chesscom::UserData> UserManager::GetUserData(const std::string& token)
{
    std::scoped_lock<std::mutex> lock(_mutex);
    if(_logedInUsers.count(token) > 0){
        return _logedInUsers.at(token).data;
    } 
    else {
        return std::shared_ptr<chesscom::UserData>();
    }
}

chesscom::UserData UserManager::GetPublicUserDataFromUsername(const std::string& username)
{
    {
        std::scoped_lock<std::mutex> lock(_mutex);
        for (auto& [usertoken, user] : _logedInUsers)
        {
            if(user.data->username() == username){
                chesscom::UserData userdata;
                userdata.set_elo(user.data->elo());
                userdata.set_username(user.data->username());
                return userdata;
            }   
        }
    }
    chesscom::UserData userData;\
    const auto filename = UserStore::UsernameToDataFilename(username);
    if(Filesystem::UserFileExists(filename)){
        try{
            auto userdata_static = Filesystem::ReadUserFile(UserStore::UsernameToDataFilename(username));
            userData.set_username(userdata_static.username());
            userData.set_elo(userdata_static.elo());
            return userData;

        } catch(std::invalid_argument& ex){

        }
    }
    Logger::log(Logger::LogLevel::Warning,"Failed to find userdata for '"+username+"'");
    return userData;
}

User* UserManager::GetUser(const std::string& token)
{
    std::scoped_lock<std::mutex> lock(_mutex);
    if(_logedInUsers.count(token) > 0){
        return &_logedInUsers.at(token);
    } 
    else {
        return nullptr;
    }
}
std::string UserManager::GetUserName(const std::string& token)
{
    std::scoped_lock<std::mutex> lock(_mutex);
    if(_logedInUsers.count(token) > 0){
        return _logedInUsers.at(token).data->username();
    } 
    else {
        return "{unknown username}";
    }
}
const std::vector<unsigned char>& UserManager::GetEncryptionKey(const std::string& token)
{
    std::scoped_lock<std::mutex> lock(_mutex);
    if(_logedInUsers.count(token) > 0){
        return _logedInUsers.at(token).encryptionKey;
    } 
    else {
        throw std::invalid_argument("token is not a loggen in user -> "+ token);
    }
}

std::pair<duration,duration> UserManager::GetUserPingAndOffset(const std::string& token)
{
    std::scoped_lock<std::mutex> lock(_mutex);
    if(_logedInUsers.count(token) > 0){
        return {_logedInUsers.at(token).ping_value, _logedInUsers.at(token).estimated_clock_offset};
    } 
    else {
        throw std::invalid_argument("token is not a logged in user -> "+ token);
    }
}

void UserManager::CheckHeartbeat()
{
    std::vector<std::string> usersToLogOut;
    {
    std::unique_lock<std::mutex> scopeLock (_mutex);
    for(auto&& keyVal : _logedInUsers)
    {
        User& user = keyVal.second;
        if(std::chrono::system_clock::now() > user.lastHeartbeat + std::chrono::minutes(2))
        {
            usersToLogOut.push_back(keyVal.first);
        }
    }
    }
    for(auto&& usertoken : usersToLogOut)
    {
        Logout(usertoken);
    }
}
void UserManager::CheckPing()
{
    std::unique_lock<std::mutex> scopeLock (_ping_mutex);
    auto copy = _ping_service_users;
    for(auto& [usertoken, ping_data] : copy)
    {
        if(!ping_data->alive.expired()){
            auto locked_alive = ping_data->alive.lock();
            if(ping_data->pings_sent.size() == 0){
                //Logger::log(Logger::LogLevel::Debug,"Server requesting ping for '"+usertoken+"'");
                auto token = generate_random_uint64();
                chesscom::PingRequest req;

                std::scoped_lock<std::mutex> lock(_mutex);
                if(_logedInUsers.count(usertoken) > 0){
                    req.set_current_ping(std::chrono::duration_cast<std::chrono::milliseconds>(_logedInUsers.at(usertoken).ping_value).count());
                    req.set_current_clock_offset(std::chrono::duration_cast<std::chrono::milliseconds>(_logedInUsers.at(usertoken).estimated_clock_offset).count());
                } 
                req.set_token(token);
                auto start_time = timeNow();
                //Logger::log(Logger::Info, fmt::format("Start time {}", start_time.time_since_epoch()));
                ping_data->stream->Write(req);
                ping_data->pings_sent[token] = start_time;
            } else {
                auto cp_map = ping_data->pings_sent;
                for (auto& [token, time_point] : cp_map){
                    if(timeNow() - time_point > std::chrono::seconds(2)){
                        ping_data->pings_sent.erase(token);
                    }
                }
            }
        }else{
            _ping_service_users.at(usertoken)->stream = nullptr;
            _ping_service_users.erase(usertoken);
            Logger::log(Logger::LogLevel::Debug,fmt::format("Server removed '{}' from ping service as stream is expired",usertoken));
        }

        
        //ptr->Read()
    }
}
void UserManager::UpdateElo(const std::string& white_usertoken, const std::string& black_usertoken, chesscom::MatchEvent result)
    {
        if(result != chesscom::MATCH_EVENT_WHITE_WIN &&
            result != chesscom::MATCH_EVENT_BLACK_WIN &&
            result != chesscom::MATCH_EVENT_DRAW)
        {
            return;
        }
        auto white = GetUser(white_usertoken);
        auto black = GetUser(black_usertoken);
        auto result_float = result==chesscom::MATCH_EVENT_BLACK_WIN?1.0f:result==chesscom::MATCH_EVENT_DRAW?0.5f:0.0f;
        if(white != nullptr && black != nullptr){
            std::unique_lock<std::mutex> scopeLock (_mutex);
            auto [white_elo, black_elo] = UserStore::Get()->CalcNewElo(white->data->elo(), black->data->elo(), result_float);
            white->data->set_elo(white_elo);
            black->data->set_elo(black_elo);
            auto now = timeNow();
            white->lastDataUpdate = now;
            black->lastDataUpdate = now;
        }else{
            Logger::log(Logger::LogLevel::Error,"Failed to update elo as user no longer logged in");
        }
        
    }

bool UserManager::AddUserToPingService(const std::string& usertoken, grpc::ServerReaderWriter<chesscom::PingRequest,chesscom::PingResponse>* stream, std::weak_ptr<bool> alive){
    if(_logedInUsers.count(usertoken) > 0){
        std::unique_lock scopeLock (_ping_mutex);
        if(_ping_service_users.count(usertoken) < 1){
            _ping_service_users.emplace(usertoken, std::make_shared<PingData>(stream, alive));
            return true;
        }
    }
    return false;
}

void UserManager::AddMessageStream(const std::string& usertoken, grpc::internal::WriterInterface< chesscom::ChatMessage>* stream) {
    std::unique_lock scopeLock (_mutex);
    if(_logedInUsers.count(usertoken) > 0){
        std::unique_lock scopeLock (_logedInUsers.at(usertoken).messageStreamMutex);
        _logedInUsers.at(usertoken).messageStream = stream;
    } 
}
void UserManager::RemoveMessageStream(const std::string& usertoken)
{
    std::unique_lock scopeLock (_mutex);
    if(_logedInUsers.count(usertoken) > 0){
        std::unique_lock scopeLock (_logedInUsers.at(usertoken).messageStreamMutex);
        _logedInUsers.at(usertoken).messageStream = nullptr;
    } 
}
