/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#include "databasemanager.h"
#include "google/protobuf/util/time_util.h"

DatabaseManager* DatabaseManager::_instance = 0;

void DatabaseManager::DoWork(){
    {std::scoped_lock lock(_mutex);
        _query_results.remove_if([](auto x){ return x->timestamp < timeNow() - std::chrono::minutes(10); });
    }
}

bool compare_game_filters(chesscom::SearchFilter filter1, chesscom::SearchFilter filter2){
    if(filter1.username() != filter2.username()) return false;
    if(filter1.has_after_timestamp() != filter2.has_after_timestamp()) return false;
    if(filter1.after_timestamp() != filter2.after_timestamp()) return false;
    if(filter1.has_before_timestamp() != filter2.has_before_timestamp()) return false;
    if(filter1.before_timestamp() !=  filter2.before_timestamp()) return false;
    return true; 
}


chesscom::GamesDatabaseResult DatabaseManager::QueryGamesDatabase(const chesscom::GameSearch& query)
{
    chesscom::GamesDatabaseResult result;
    std::shared_ptr<QueryResult> wanted_query = nullptr;
    {std::scoped_lock lock(_mutex);
        auto any_equal = std::find_if(_query_results.begin(),_query_results.end(), [ &query = query](std::shared_ptr<QueryResult>& x){ return compare_game_filters(x->query.search_filter(), query.search_filter()); });
        if(any_equal != _query_results.end()){
            wanted_query = *any_equal;
        }
    }
    if(wanted_query == nullptr){
        auto games = Filesystem::GetListOfGamesDatabase();
        if(query.search_filter().username() != ""){
            std::vector<chesscom::PgnInfo> games_filtered;
            std::copy_if(games.begin(), games.end(), std::back_inserter(games_filtered), [&username = query.search_filter().username()](chesscom::PgnInfo& x){ return x.white_username() == username || x.black_username() == username; });
            games.swap(games_filtered);

        }
        // add filter for timestamp after and before

        {std::scoped_lock lock(_mutex);          
            auto any_equal = std::find_if(_query_results.begin(),_query_results.end(), [ &query = query](std::shared_ptr<QueryResult>& x){ return compare_game_filters(x->query.search_filter(), query.search_filter()); });
            // if the same query resently done overwrite it. Very unlikely case.
            if(any_equal == _query_results.end()){
                _query_results.push_back(std::make_shared<QueryResult>());
                wanted_query = _query_results.back();
            }else {
                wanted_query = *any_equal;
            }
            wanted_query->id = generate_random_alphanum(16);
            wanted_query->query = query;
            wanted_query->timestamp = timeNow();
            wanted_query->games = games;
        }
    }
    result.mutable_search()->mutable_search_handle()->set_id(wanted_query->id);
    *result.mutable_search()->mutable_search_handle()->mutable_timestamp() = google::protobuf::util::TimeUtil::TimevalToTimestamp(timePointToTimeval(wanted_query->timestamp));
    result.mutable_search()->set_number_of_pages(static_cast<int>(wanted_query->games.size() / DATABASE_MANAGER_GAMES_PAGE_SIZE) + 1);
    result.mutable_search()->set_this_page_number(0);
    return result;
}

chesscom::GamesDatabaseResult DatabaseManager::GamesDatabaseFetch(const chesscom::GamesDatabaseFetch& fetch){
    chesscom::GamesDatabaseResult result;
    {std::scoped_lock lock(_mutex);  
        auto any_equal = std::find_if(_query_results.begin(),_query_results.end(), [ &search_handle = fetch.search_handle()](auto x){ return x->id == search_handle.id(); });
        if(any_equal != _query_results.end()){
            auto game = *any_equal;
            result.mutable_search()->mutable_search_handle()->CopyFrom(fetch.search_handle());
            result.mutable_search()->set_number_of_pages(static_cast<int>(game->games.size() / DATABASE_MANAGER_GAMES_PAGE_SIZE) + 1);
            result.mutable_search()->set_this_page_number(fetch.page_number());
            auto start = game->games.begin() + (fetch.page_number() - 1) * DATABASE_MANAGER_GAMES_PAGE_SIZE;
            auto end = static_cast<uint32_t>(fetch.page_number() * DATABASE_MANAGER_GAMES_PAGE_SIZE) >= game->games.size()?game->games.end():
                    game->games.begin() + fetch.page_number()*DATABASE_MANAGER_GAMES_PAGE_SIZE;
            //result.mutable_search()->mutable_games(std::distance(start, end));
            //std::vector<std::string> texts;
            // std::transform(start, end,                              // input iterators
            //         std::back_inserter(texts),                      // output iterators (inserts at the end of texts)
            //         [](const PgnInfo& x) { return x.filename; });   // lambda which maps A to std::string
            result.mutable_search()->mutable_games()->Add(start, end);
        } else {
            *result.mutable_failed_message() = "Search handle no longer available";
        }
    }
    return result;
}