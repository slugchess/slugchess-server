/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#include "filesystem.h"

std::string Filesystem::_root_dir_cached = "";
std::string Filesystem::_data_dir = "";

std::string Filesystem::ExeDir(){
        if(_root_dir_cached == ""){
        char buf[4096 + 1];
        if (readlink("/proc/self/exe", buf, sizeof(buf) - 1) == -1)
        throw std::string("readlink() failed");
        std::string str(buf);
        _root_dir_cached = str.substr(0, str.rfind('/')+1); //Retuns up to and including last '/'
        return _root_dir_cached;
    } else {
        return _root_dir_cached;
    }
}

std::string Filesystem::DataDir(){
    if(_data_dir == ""){
        return ExeDir();
    } else {
        return _data_dir;
    }
}

void Filesystem::SetDataDir(const std::string path){
    std::filesystem::create_directories(path);
    if(std::filesystem::is_directory(path)){
        _data_dir = std::filesystem::canonical(path).string() + "/";
        Logger::log(Logger::LogLevel::Info,fmt::format("Data dir set to '{}'", _data_dir));
    }else{
        Logger::log(Logger::LogLevel::Info,fmt::format("Data dir could not change from '{}' to '{}'", _data_dir, path));
    }
}

void Filesystem::WriteMatchPgn(const std::string userWhite, const std::string& userBlack, const std::string& pgn, time_t& time_t)
{
    std::ofstream pgnFile;
    std::filesystem::create_directories(DataDir() + "games_database");
    auto clean_white = userWhite;
    clean_white.erase(std::remove(clean_white.begin(), clean_white.end(), '/'), clean_white.end());
    clean_white.erase(std::remove(clean_white.begin(), clean_white.end(), '\0'), clean_white.end());
    clean_white.erase(std::remove(clean_white.begin(), clean_white.end(), '_'), clean_white.end());
    auto clean_black = userBlack;
    clean_black.erase(std::remove(clean_black.begin(), clean_black.end(), '/'), clean_black.end());
    clean_black.erase(std::remove(clean_black.begin(), clean_black.end(), '\0'), clean_black.end());
    clean_black.erase(std::remove(clean_black.begin(), clean_black.end(), '_'), clean_black.end());
    std::stringstream ss;ss << DataDir() << "games_database/" << std::put_time(std::gmtime(&time_t), DATETIME_FORMAT) << "_" << clean_white << "_" << clean_black << ".pgn";
    pgnFile.open (ss.str(), std::ios::out | std::ios::trunc);
    if(pgnFile.is_open()){
        pgnFile << pgn;
        //Logger::log(Logger::LogLevel::Debug,"Wrote pgn to '" + ss.str() + "' " + pgn );
    }
    
    pgnFile.close();
}

std::vector<chesscom::PgnInfo> Filesystem::GetListOfGamesDatabase()
{
    std::vector<std::future<std::shared_ptr<chesscom::PgnInfo>>> games_future;
    auto dir_it = std::filesystem::directory_iterator(DataDir() + "games_database");
    auto func = [](const std::filesystem::path file) -> std::shared_ptr<chesscom::PgnInfo> {
        try { 
            return std::make_shared<chesscom::PgnInfo>(PgnPathToInfo(file)); 
        }
        catch(std::string error){
            Logger::log(Logger::LogLevel::Warning, fmt::format("PgnPath to info '{}' failed: {}", file.filename().string(), error));
            return nullptr;
        }
    };
    std::vector<chesscom::PgnInfo> games;
    for(const auto& file : dir_it){
        if(file.is_regular_file()) {
            games_future.push_back(Threadpool::pool.push(func, file.path()));
        }
    }
    for(auto& future : games_future){
        std::shared_ptr<chesscom::PgnInfo> ptr = future.get();
        if(ptr != nullptr){
            games.push_back(*ptr);
        }
    }
    return games;
}

std::string Filesystem::GetPgnFromGamesDatabase(const std::string& filename)
{
    auto dir_it = std::filesystem::directory_iterator(DataDir() + "games_database");
    for(const auto& file : dir_it){
        if(file.path().stem() == filename){
            return ReadFile(file.path());
        }
    }
    return "";
}

std::string Filesystem::ReadFile(const std::filesystem::path& path){
    std::ifstream file;
    file.open (path, std::ios::in | std::ios::binary);
    const auto sz = std::filesystem::file_size(path);
    std::string game_pgn(sz, '\0');
    file.read(game_pgn.data(), sz);
    return game_pgn;
}

chesscom::PgnInfo Filesystem::PgnPathToInfo(const std::filesystem::path& path)
{
    chesscom::PgnInfo pgn;
    pgn.set_filename(path.stem());
    //auto tokens = tokenize(path.stem(), "_");
    auto pgnss = ReadSlugChessPgnString(ReadFile(path));
    time_t datetime;
    if(!ReadPgnTime(pgnss["Date"]+pgnss["Time"], datetime)){
        throw std::string("Could not read datetime: " + pgnss["Date"]+pgnss["Time"]);
    }
    pgn.mutable_timestamp()->MergeFrom(google::protobuf::util::TimeUtil::TimeTToTimestamp(datetime));
    pgn.set_white_username(pgnss["White"]);
    pgn.set_black_username(pgnss["Black"]);
    return pgn;
}

bool Filesystem::UserFileExists(const std::string& filename)
{
    std::ifstream userDataFile;
    std::stringstream ss;ss << DataDir() << "user_database/" << filename;
    userDataFile.open (ss.str());
    return userDataFile.is_open();
}

void Filesystem::WriteUserFile(chesscom::UserStaticData& userdata, const std::string& filename)
{
    //std::lock_guard<std::mutex> lock(_user_mutex);
    std::ofstream userFile;
    std::filesystem::create_directories(DataDir() + "user_database");
    std::stringstream ss;ss << DataDir() << "user_database/" << filename;
    userFile.open (ss.str(), std::ios::out | std::ios::trunc);
    if(userFile.is_open()){

        std::string out;
        auto status = google::protobuf::util::MessageToJsonString(userdata, &out);
        if(status.ok()){
            userFile.write(out.data(), out.size());
            Logger::log(Logger::LogLevel::Debug,"Wrote userdata to '" + ss.str() + "' " + out);
        }else{
            Logger::log(Logger::LogLevel::Error,"Something falsed when turning UserStaticData to JSON." + std::string(status.message()));
            throw std::invalid_argument("encryption of userdata failed");
        }
        
    }else{
        Logger::log(Logger::LogLevel::Error,"Failed to open user data file '" + ss.str() + "'. Failed to save userdata");
        throw std::invalid_argument("encryption of userdata failed");
    }
    
    userFile.close();
}

chesscom::UserStaticData Filesystem::ReadUserFile(const std::string& filename)
{
    std::ifstream userDataFile;
    std::stringstream ss;ss << DataDir() << "user_database/" << filename;
    auto path = ss.str();
    OpenFileWithAttemps(&path, &userDataFile, std::ios::in | std::ios::binary | std::ios::ate ); // ate to move to end of file
    //Logger::log(Logger::LogLevel::Info,"opening " + ss.str() + " is opem " + std::to_string(userDataFile.is_open()));
    if(userDataFile.is_open()){
        std::ifstream::pos_type pos = userDataFile.tellg();
        std::vector<unsigned char>  file_data(pos);
        userDataFile.seekg(0, std::ios::beg);
        userDataFile.read((char*)file_data.data(), pos); //Should be fine to cast
        std::string data_string((char*)file_data.data(), pos);

        chesscom::UserStaticData de;
        auto status = google::protobuf::util::JsonStringToMessage(data_string, &de);
        if(!status.ok()){
            Logger::log(Logger::LogLevel::Error, std::string(status.message()));
        }
        userDataFile.close();
        return de;
    }
    userDataFile.close();
    Logger::log(Logger::LogLevel::Error,"Failed to open userdata file. " + filename);
    throw std::invalid_argument("filename failed to be opened: "+ filename);
}

bool Filesystem::OpenFileWithAttemps(const std::string* filepath, std::ifstream* dataFile, const std::ios_base::openmode openmode){
    int open_attempts = 6;
    int sleep_ms = 3;
    bool file_open = false;
    while (!file_open && open_attempts > 0)
    {
        open_attempts--;
        dataFile->open(*filepath, openmode);
        if(dataFile->is_open()) break;
        std::this_thread::sleep_for(std::chrono::milliseconds(sleep_ms));
    }
    return dataFile->is_open();
}
