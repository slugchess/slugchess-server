/*
SlugChess Server is a server for SlugChess variants
Copyright (C) 2022 Spaceslug(spaceslug@slugchess.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/
#pragma once
#include <map>
#include <vector>
#include <set>
#include <algorithm>

#include <google/protobuf/util/time_util.h>

#include "../chesscom/chesscom.grpc.pb.h"
#include "slugchess.h"
#include "common.h"
#include "match.h" 

class SlugChessConverter
{
    public:
    static void SetGameState(std::shared_ptr<SlugChess> game, chesscom::GameState* gameState, PlayerTypes playerType){
        //auto vision = isWhitePlayer?game->GetWhiteVision():game->GetBlackVision(); 
        auto pieces = game->GetPieces(); 
        auto captured = game->KilledPieces();
        std::vector<chesscom::PieceCapture> capturedTrans{captured->size()}; 
        std::transform (captured->begin(), captured->end(), capturedTrans.begin(), [](std::pair<ChessPiece,int> cp)
        {
            chesscom::PieceCapture newCP;
            newCP.set_piece((chesscom::Pieces)cp.first);
            newCP.set_location(SlugChess::BP(cp.second));
            return newCP;
        });
        auto vision_state = gameState->mutable_vision_state();
        switch (playerType)
        {
        case PlayerTypes::White:
            {
                auto wv = game->GetWhiteVision();
                *vision_state->mutable_white_vision() = {wv.begin(), wv.end()};
                CopyToMap(gameState->mutable_player_moves(), game->LegalWhiteMovesRef());
                CopyToMap(gameState->mutable_shadow_moves(), game->ShadowBlackMovesRef());
            }
            break;
        case PlayerTypes::Black:
            {
                auto bv = game->GetBlackVision();
                *vision_state->mutable_black_vision() = {bv.begin(), bv.end()};
                CopyToMap(gameState->mutable_player_moves(), game->LegalBlackMovesRef());
                CopyToMap(gameState->mutable_shadow_moves(), game->ShadowWhiteMovesRef());
            }
            break;
        case PlayerTypes::Observer:
            {
                auto wv = game->GetWhiteVision();
                auto bv = game->GetBlackVision();
                *vision_state->mutable_white_vision() = {wv.begin(), wv.end()};
                *vision_state->mutable_black_vision() = {bv.begin(), bv.end()};
                CopyToMap(gameState->mutable_player_moves(), game->WhitesTurn()?game->LegalWhiteMovesRef():game->LegalBlackMovesRef());
                CopyToMap(gameState->mutable_shadow_moves(), game->WhitesTurn()?game->ShadowBlackMovesRef():game->ShadowWhiteMovesRef());
            }
            break;
        }
        //*gameState->mutable_player_vision() = {vision.begin(), vision.end()};
        *gameState->mutable_pieces() = {pieces.begin(), pieces.end()};
        *gameState->mutable_captured_pieces() = {capturedTrans.begin(), capturedTrans.end()};
        //gameState->set_captured_piece((chesscom::Pieces)game->LastCaptured());
        gameState->set_from(game->From(static_cast<SlugChess::Perspective>(playerType)));
        gameState->set_to(game->To(static_cast<SlugChess::Perspective>(playerType)));
        gameState->set_current_turn_is_white(game->WhitesTurn());
        //MOves



        auto check = gameState->mutable_check();
        check->Clear();
        for (auto &&index : game->Checks(static_cast<SlugChess::Perspective>(playerType)))
        {
            check->Add(SlugChess::BP(index)); 
        }
    }
private:
    static void CopyToMap(google::protobuf::Map<std::string,chesscom::FieldMoves> *to, 
                            std::map<int, std::vector<int>> *from)
    {
        chesscom::FieldMoves fm;
        auto fmRF = fm.mutable_list();
        for (auto &&keyVal : *from)
        {
            for (auto &&pos : keyVal.second)
            {
                fmRF->Add(SlugChess::BP(pos));
            }
            (*to)[SlugChess::BP(keyVal.first)].CopyFrom(fm);    
            fmRF->Clear();
        }
    }
};