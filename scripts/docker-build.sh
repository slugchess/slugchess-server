#!/bin/bash
DOCKER_BUILDKIT=1 sudo -E docker build -t grpc-env:latest -f ./Dockerfile.grpc .
DOCKER_BUILDKIT=1 sudo -E docker build -t slugchess:latest -f ./Dockerfile.server .
DOCKER_BUILDKIT=1 sudo -E docker build -t slugchess-dbg:latest -f ./Dockerfile.server-debug .
