If you want to contibute contact Spaceslug <me@spaceslug.no> and I will write an actual CONTRIBUTING guide.

git submodule update --init --recursive

Scripts in scripts folder are assumed to be ran from the project root folder.

# Header
Using [https://github.com/okdshin/PicoSHA2] for sha-256
Using [https://github.com/kkAyataka/plusaes] for AES.
Using [https://github.com/nlohmann/json] for json serialization.
Using [https://github.com/cardsorg/Elo/blob/master/elo.hpp] for elo calulation
Using [https://github.com/bloomen/cxxpool] for thread pool

ObjectBox database?

# debug stuff
[https://github.com/rogchap/wombat](Wombat) is a nice gui tool for debuging the grpc service. 